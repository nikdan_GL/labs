.PHONY : clean, all, clean_all

PWD = $(shell pwd)
CC = gcc

all:
	-mkdir bin lib
	-$(MAKE) -C $(PWD)/include/
	-$(MAKE) -C $(PWD)/lab1/
	-$(MAKE) -C $(PWD)/tex1/
	-$(MAKE) -C $(PWD)/planet/
	-$(MAKE) -C $(PWD)/light/

#	-rm -rf bin/ lib/
#
#
clean:
	-$(MAKE) -C $(PWD)/include clean
	-$(MAKE) -C $(PWD)/lab1/ clean
	-$(MAKE) -C $(PWD)/tex1/ clean
	-$(MAKE) -C $(PWD)/planet/ clean
	-$(MAKE) -C $(PWD)/light/ clean


clean_all: clean
	$(MAKE) -C $(PWD)/include clean_all
	rmdir bin
	rmdir lib
