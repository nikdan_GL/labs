
#include "planet.h"
#include <math.h>

static double year = 0, day = 0;
const float pi = 3.14;

void init()
{
	glClearColor( 0.0, 0.0, 0.0, 0.0 );
	glShadeModel( GL_FLOAT );
}

void display()
{
	glLoadIdentity();
	gluLookAt( 1.0, 2.0, 5.0,
	           .0, .0, .0,
	           .0, 1.0, .0);

	glClear (GL_COLOR_BUFFER_BIT );


	glPushMatrix();
	glColor3f( 1.0, 1.0, .0 );
	glRotatef( year/3, .0, 1.0, .0 );
	glRotatef( 90, 1.0, .0, .0 );
	glutWireSphere( .5, 20, 16 );
	glPopMatrix();


  float vect_x0 = -1.0;
  float vect_y0 = 0.0;
  float vect_z0 = 0.0;

	glPushMatrix();
	glColor3f( .2, .2, 1.0 );
	glRotatef( year, .0, 1.0, .0 );
	glTranslatef( 2.0, 0.0, 0.0 );
//	glRotatef( day, .0, 0.0, -1.0 );
	glBegin( GL_LINES );
	{
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(vect_x0, vect_y0, vect_z0);
	}
  glEnd();

  glRotatef( year, vect_x0, vect_y0, vect_z0 );
  glScalef(0.2, 0.2, 0.2);
	glTranslatef( .0, .0, -2.5 );
	glutWireCylinder( 1.0, 5.0, 10, 10);
	glPopMatrix();

	glutSwapBuffers();
}

void reshape(int w, int h)
{
	glViewport( 0, 0, w, h );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( 60.0, (GLdouble)w / (GLdouble)h, 1.0, 20.0 );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	/* gluLookAt( 1.0, 2.0, 5.0, */
	/*            .0, .0, .0, */
	/*            .0, 1.0, .0); */
}

void keyboard( unsigned char key, int x, int y )
{
	switch (key) {
	case 'd':
		day = (int)(day + 10) % 360;
		break;
	case 'D':
		day = (int)(day - 10) % 360;
		break;
	case 'y':
		year = (int)(year + 5) % 360;
		break;
	case 'Y':
		year = (int)(year - 5) % 360;
		break;
	case 'q':
		exit(0);
	}
	glutPostRedisplay();
}


int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB );
	glutInitWindowSize( 500, 500 );
	glutInitWindowPosition( 100, 100 );
	glutCreateWindow( argv[0] );

	init();

	glutDisplayFunc( display );
	glutReshapeFunc( reshape );
	glutKeyboardFunc( keyboard );
	glutMainLoop();
	return 0;
}
