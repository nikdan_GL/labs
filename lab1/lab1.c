
#include "lab1.h"
#include "viewer.h"
#include <math.h>
#include <stdio.h>

const unsigned int start_width = 700;
const unsigned int start_height = 700;

const unsigned int start_position_x = 100;
const unsigned int start_position_y = 100;

struct tetrahedron_t tetrahedron;
struct sphere_t sphere;

struct rotation_t task_rotation_hedron;
struct rotation_t task_rotation_sphere;

struct viewer_t * viewer;


void reprint()
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	const double * hvd = get_hvd_view( viewer );
	glOrtho( -hvd[0]/2, hvd[0]/2, -hvd[1]/2, hvd[1]/2, -5.0, hvd[2] );

	glMatrixMode( GL_MODELVIEW );
}


void init()
{
	glClearColor( 0.0, 0.0, 0.0, 0.0 );
	glShadeModel( GL_FLOAT );

	tetrahedron.center = (struct position_t){.0, .0, -1.0};
	tetrahedron.rotation.normal = (struct position_t){.0, .5, .0};
	tetrahedron.rotation.angle = 3.0;

	sphere.center = (struct position_t){.0, -0.5, 3.0};
	sphere.radius = 1.2;
	sphere.slices = 10;
	sphere.stacks = 10;

	task_rotation_hedron.angle = 0;
	task_rotation_hedron.normal = (struct position_t){.0, .0, .0};

	task_rotation_sphere.angle = 0;
	task_rotation_sphere.normal = (struct position_t){.0, .0, .0};

	viewer = init_view( 0.5 );
}


void display()
{
	reprint();

	const double * xyz = get_xyz_view( viewer );
	glLoadIdentity();
	gluLookAt( xyz[0], xyz[1], xyz[2],
	           0.0, 0.0, 0.0,
	           0, 1, 0 );


	glClear( GL_COLOR_BUFFER_BIT );

	glPushMatrix();
	glBegin( GL_LINES );
	{
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(2.0, 0.0, 0.0);

		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 2.0, 0.0);

		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, 0.0, 2.0);
	}
	glEnd();
	glPopMatrix();

	struct position_t center = tetrahedron.center;
	struct rotation_t rotation = tetrahedron.rotation;

	glPushMatrix();
	{
		glRotatef( task_rotation_hedron.angle,
		           task_rotation_hedron.normal.x,
		           task_rotation_hedron.normal.y,
		           task_rotation_hedron.normal.z );

		glTranslatef( center.x, center.y, center.z );
		glRotatef( rotation.angle, rotation.normal.x, rotation.normal.y, rotation.normal.z);
		glScalef( 1.0, 1.2, 1.0 );
		glutWireTetrahedron();
	}
	glPopMatrix();


	glPushMatrix();
	{
		glRotatef( task_rotation_sphere.angle,
		           task_rotation_sphere.normal.x,
		           task_rotation_sphere.normal.y,
		           task_rotation_sphere.normal.z );
		center = sphere.center;
		glTranslatef( center.x, center.y, center.z );
		glutWireSphere( sphere.radius, sphere.slices, sphere.stacks );
	}
	glPopMatrix();

	glutSwapBuffers();
}


void reshape ( int w, int h )
{
	glViewport( 0, 0, (GLsizei)w, (GLsizei)h );
}


void keyboard ( unsigned char key, int x, int y )
{
	modify_view( viewer, key );

	switch (key) {
	case 'n': {
		task_rotation_hedron.normal = (struct position_t){1.0, .0, .0};
		task_rotation_hedron.angle += -10;

		task_rotation_sphere.normal = (struct position_t){.0, .0, 1.0};
		task_rotation_sphere.angle += -5;
		break;
	}
	case 'q':
		glutLeaveMainLoop();
	}
	glutPostRedisplay();
}



int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB );
	glutInitWindowSize( start_width, start_height );
	glutInitWindowPosition( start_position_x, start_position_y );
	glutCreateWindow( argv[0] );
	init();
	glutDisplayFunc( display );
	glutReshapeFunc( reshape );
	glutKeyboardFunc( keyboard );
	glutMainLoop();
	return 0;
}
