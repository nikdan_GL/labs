#ifndef LAB1_LAB1_H
#define LAB1_LAB1_H

#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>


struct position_t {
	GLdouble x, y, z;
};

struct rotation_t {
	struct position_t normal;
	GLdouble angle;
};

struct tetrahedron_t {
	struct position_t center;
	struct rotation_t rotation;
};


struct sphere_t {
	struct position_t center;
	GLdouble radius;
	GLint slices;
	GLint stacks;
};


#endif /* LAB1_LAB1_H */
