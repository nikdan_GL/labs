
#include "viewer.h"
#include "listdir.h"

#include <GL/freeglut.h>
#include <SDL/SDL.h>
#include <string.h>

const int max_textures = 20;
const char rsrc_dir[] = "/home/nikdan/Projects/GL/labs/resources";
struct list_t * listFileName = NULL;
GLuint * TexNames = NULL;

const unsigned int start_width = 900;
const unsigned int start_height = 800;

const unsigned int start_position_x = 100;
const unsigned int start_position_y = 100;

#define LINE_SIZE  0.5
static GLfloat torus_radius = 1.5 * LINE_SIZE;
static GLfloat torus_ratio = 0.25;

static int numb_text = 0;
static int max_numb_text;

static struct viewer_t * viewer;


size_t last_power(int value)
{
	int power = 0;
	if (0 <= value) {
		power = 2;
		while (power <= value) {
			power *= 2;
		}
		power /= 2;
	}
	return power;
}


void reprint()
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	const double * hvd = get_hvd_view( viewer );
	glOrtho( -hvd[0]/2, hvd[0]/2, -hvd[1]/2, hvd[1]/2, -5.0, hvd[2] );

	glMatrixMode( GL_MODELVIEW );
}


void textureInit()
{
	struct list_t *head = list_dir(rsrc_dir);
	size_t rsrc_dir_size = sizeof(rsrc_dir) / sizeof(rsrc_dir[0]);

	const char * file_name;
	char * global_file_name;
	const int rsrc_amount = get_amount( head );

	max_numb_text = -1;
	TexNames = (GLuint*)malloc( sizeof(GLuint)*rsrc_amount );
	for (int iter = 0; (iter != rsrc_amount) && (iter != max_textures); ++iter) {
		if ((file_name = (const char*)get(head, iter))) {
			int str_size = rsrc_dir_size + strlen(file_name) + 10;
			global_file_name = (char*)malloc( sizeof(char)*str_size );
			strcpy( global_file_name, rsrc_dir );
			strcat( global_file_name, "/");
			strcat( global_file_name, file_name );

			SDL_Surface *textureImage;
			if ( ( textureImage = SDL_LoadBMP( global_file_name ) ) )
			{
				++max_numb_text;
				glGenTextures( 1, &TexNames[iter] );
				glBindTexture( GL_TEXTURE_2D, TexNames[iter] );

				gluBuild2DMipmaps(GL_TEXTURE_2D, 3,
				                  textureImage->w,
				                  textureImage->h,
				                  GL_BGR, GL_UNSIGNED_BYTE,
				                  textureImage->pixels);

				SDL_FreeSurface(textureImage);

				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );

				glTexGeni( GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );
				glTexGeni( GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );
				glTexGeni( GL_R, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );
				glTexGeni( GL_Q, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR );

				glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
			}
		}
	}

	clear(head);
}



void init()
{
	glClearColor( .0, .0, .0, .0 );
	glEnable( GL_DEPTH_TEST );

	glShadeModel( GL_SMOOTH );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	textureInit();
	viewer = init_view( 0.1 );
}


void display()
{
	reprint();

	const double * xyz = get_xyz_view( viewer );
	glLoadIdentity();
	gluLookAt( xyz[0], xyz[1], xyz[2],
	           0.0, 0.0, 0.0,
	           0, 1, 0 );


	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	glEnable(GL_TEXTURE_2D);

	/* Torus */
	glPushMatrix();
	{
		glBindTexture( GL_TEXTURE_2D, TexNames[numb_text] );
		glRotatef(35.0, .0, 1.0, .5);
		if (torus_ratio < 5) {
			glutSolidTorus( torus_radius*torus_ratio, torus_radius, 100, 100 );
		}
		else {
			glutSolidSphere( torus_ratio*torus_radius, 100, 100 );
		}
	}
	glPopMatrix();

	glPushMatrix();
	{
		glBindTexture( GL_TEXTURE_2D, TexNames[0] );
		glRotatef(45.0, 1.0, 0.0, 1.0);
		glTranslatef( 0.5*LINE_SIZE, 0.5*LINE_SIZE, 0.5*LINE_SIZE);
		glutSolidCube( LINE_SIZE );
	}
	glPopMatrix();

	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_2D);

	glutSwapBuffers();
}


void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glLoadIdentity();
}


void keyboard ( unsigned char key, int x, int y )
{
	modify_view( viewer, key );

	switch (key) {
	case 'q':
		glutLeaveMainLoop();
		break;
	case 'n':
		torus_ratio *= 1.1;
		break;
	case 'p':
		torus_ratio /= 1.1;
		break;
	case 'N' :
		++numb_text;
		if (max_numb_text < numb_text) {
			numb_text = 0;
		}
		break;
	case 'P' :
		--numb_text;
		if (numb_text < 0) {
			numb_text = max_numb_text;
		}
		break;
	}
	glutPostRedisplay();
}


int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowSize( start_width, start_height );
	glutInitWindowPosition( start_position_x, start_position_y );
	glutCreateWindow(argv[0]);

	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}
