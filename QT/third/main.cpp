
#include "mainwindow.h"
#include "glextensions.h"

#include <QApplication>
#include <QGuiApplication>
#include <QPainter>
#include <QGraphicsView>

#include <vector>

bool checkVersion(QOpenGLContext &context, QSurfaceFormat &format);
QSurfaceFormat* getFirstSupported(std::vector<QSurfaceFormat> &formats);
bool matchString(const char *extensionString, const char *subString);


class GraphicsView : public QGraphicsView
{
public:
	GraphicsView()
	{
		setWindowTitle(tr("Boxes"));
		setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
	}

protected:
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE {
		if (scene())
			scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
		QGraphicsView::resizeEvent(event);
	}
};


int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	std::vector<QSurfaceFormat> formats;

	// Set OpenGL Version information
	// Note: This format must be set before show() is called.
	QSurfaceFormat glFormat;
	glFormat.setRenderableType(QSurfaceFormat::OpenGL);
	glFormat.setProfile(QSurfaceFormat::CoreProfile);
	glFormat.setVersion(3,3);
	formats.push_back(glFormat);

	QSurfaceFormat *format = getFirstSupported(formats);
	if (format == NULL)
	{
		qFatal("No valid supported version of OpenGL found on device!");
	}

	// Set the window up
	MainWindow window;
	window.setFormat(*format);
	window.resize(QSize(800, 600));
	window.show();

	return app.exec();
}





bool checkVersion(QOpenGLContext &context, QSurfaceFormat &format)
{
	QSurfaceFormat currSurface = context.format();
	QPair<int,int> currVersion = currSurface.version();
	QPair<int,int> reqVersion = format.version();
	if (currVersion.first > reqVersion.first)
		return true;
	return (currVersion.first == reqVersion.first && currVersion.second >= reqVersion.second);
}


QSurfaceFormat* getFirstSupported(std::vector<QSurfaceFormat> &formats)
{
	QOpenGLContext context;
	for (QSurfaceFormat &format : formats)
	{
		context.setFormat(format);
		if (context.create())
		{
			if (checkVersion(context, format))
			{
				return &format;
			}
		}
	}
	return NULL;
}



bool matchString(const char *extensionString, const char *subString)
{
	  int subStringLength = strlen(subString);
		return (strncmp(extensionString, subString, subStringLength) == 0)
		    && ((extensionString[subStringLength] == ' ') || (extensionString[subStringLength] == '\0'));
}
