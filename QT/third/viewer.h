#ifndef VIEWER_H
#define VIEWER_H

#include <QMatrix4x4>
#include <QVector3D>
#include <cmath>

class Viewer
{
public:
	Viewer();
	Viewer( float distance );
	Viewer( const QVector3D &xyzPosition );
	Viewer( float distStep, float angleStep, const QVector3D &xyzPosition );

	const QVector3D & getXYZ() const;
	const QMatrix4x4 & getViewMatrix() const;
	void setXYZ( const QVector3D & position );
	bool modify( unsigned char key );

	static void angle2xyz( const QVector3D * srcPosition, QVector3D * dstPosition );
	static void xyz2angle( const QVector3D * srcPosition, QVector3D * dstPosition );

	static double getDistance( const QVector3D &srcPosition );
	static double getAzimuth( const QVector3D &srcPosition );
	static double getAnglePlace( const QVector3D &srcPosition );

	static double rad2deg( double rad );
	static double deg2rad( double deg );

public:
	static const double pi;


private:
	void init( float distance );
	void init( const QVector3D &srcPosition );


private:
	float distStep;
	float angleStep;
	QVector3D xyzPosition;
	QVector3D anglePosition;  // distance, azimuth, angle_place

	QMatrix4x4 viewMatrix;
};

#endif // VIEWER_H
