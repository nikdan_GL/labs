#ifndef CUBEPOINTS_H
#define CUBEPOINTS_H

#include <vector>
#include "points.h"

class CubePoints : public Points
{
public:
	CubePoints( float sideScale );
};

#endif // CUBEPOINTS_H
