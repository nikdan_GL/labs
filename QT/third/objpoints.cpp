
#include "objpoints.h"
#include <string>
#include <fstream>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDebug>


ObjPoints::ObjPoints( const char * fileName )
{
	init(fileName);
}

ObjPoints::ObjPoints( std::string fileName )
{
	init(fileName.c_str());
}

ObjPoints::ObjPoints( const std::string & fileName)
{
	init(fileName.c_str());
}



void ObjPoints::init( const char * fileName )
{
	QString qstrFileName(fileName);
	QFile mFile(qstrFileName);

	if(!mFile.open(QFile::ReadOnly | QFile::Text)){
		  qDebug() << "could not open file for read";
			return;
	}

	QTextStream in(&mFile);
	QString text = in.readAll();
	mFile.close();

	if (loader.replaceData(text.toStdString()))
	{
		const std::vector<std::string> & nameArray = loader.getNameArray();
		vertex = loader.getDataArray(nameArray[0], ObjLoader::DataType::vertex);
		normal = loader.getDataArray(nameArray[0], ObjLoader::DataType::normal);
		texture = loader.getDataArray(nameArray[0], ObjLoader::DataType::texture);
		index = loader.getIndexArray(nameArray[0]);
		edgeSize = loader.getEdgeSize(nameArray[0]);
	}
}
