#ifndef POINTS_H
#define POINTS_H

#include <vector>

class Points
{
public:
	Points() = default;

	Points( unsigned int vertexSize,
	        unsigned int normalSize,
	        unsigned int textureSize,
	        unsigned int indexSize );

	virtual ~Points() {}

	const std::vector<float> & getVertex() const;
	const std::vector<float> & getNormal() const;
	const std::vector<float> & getTexture() const;
	const std::vector<unsigned int> getIndex() const;
	unsigned int getEdgeSize() const;

	static const unsigned int dimension = 3;

protected:
	std::vector<float> vertex;
	std::vector<float> normal;
	std::vector<float> texture;
	std::vector<unsigned int> index;

	unsigned int edgeSize;
};

#endif // POINTS_H
