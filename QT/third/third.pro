#-------------------------------------------------
#
# Project created by QtCreator 2016-10-16T21:55:06
#
#-------------------------------------------------

QT       += core gui opengl

contains(QT_CONFIG, opengles.|angle|dynamicgl):error("This example requires Qt to be configured with -opengl desktop")
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = third
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    cubepoints.cpp \
    drawable.cpp \
    points.cpp \
    spherepoints.cpp \
    lightinfo.cpp \
    material.cpp \
    viewer.cpp \
    loader.cpp \
    objpoints.cpp \
    glextensions.cpp

HEADERS  += mainwindow.h \
    cubepoints.h \
    drawable.h \
    points.h \
    spherepoints.h \
    material.h \
    lightinfo.h \
    viewer.h \
    loader.hpp \
    objpoints.h \
    glextensions.h

RESOURCES += \
    resources.qrc \
    objects.qrc

