#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "drawable.h"
#include "points.h"
#include "lightinfo.h"
#include "viewer.h"

#include <vector>

#include <QMainWindow>
#include <QOpenGLWindow>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>


class MainWindow : public QOpenGLWindow
{
	Q_OBJECT

// OpenGL Events
public:
	virtual void initializeGL() override;
	virtual void resizeGL(int width, int height) override;
	virtual void paintGL() override;

// Widget Events
	virtual void keyPressEvent( QKeyEvent * event) override;

private:
	void setMatrices( const QMatrix4x4 &modelMatrix, const QMatrix4x4 &viewMatrix );

private:
	QOpenGLFunctions *func;
	QOpenGLShaderProgram *shader_program;
	QOpenGLShader * vert_shader;
	std::vector<QOpenGLShader*> fragm_shaders;

	Viewer viewer;
	QMatrix4x4 projection;
	float angle;

	LightInfo *lightInfo;
	Points *points;
	std::vector<Drawable*> figure3D;
};

#endif // MAINWINDOW_H
