
#include "loader.hpp"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>

using std::string;


ObjLoader::ObjLoader( const std::string & strData )
{
	bool success = replaceData( strData );
	std:: cout << "out = " << success << std::endl;
	if (success)
	{
		std::cout << cashNameArray[0] << '\n';
		std::shared_ptr<Object> objectPtr = objectMap[cashNameArray[0]];

		std::cout << "vertex \n";
		for (size_t i = 0; i < objectPtr->vertexArray.size(); ++i)
		{
			std::cout << objectPtr->vertexArray[i] << '\n';
		}

		std::cout << "normal \n";
		for (size_t i = 0; i < objectPtr->normalArray.size(); ++i)
		{
			std::cout << objectPtr->normalArray[i] << '\n';
		}

		std::cout << "texture \n";
		for (size_t i = 0; i < objectPtr->textureArray.size(); ++i)
		{
			std::cout << objectPtr->textureArray[i] << '\n';
		}

		std::cout << "index \n";
		for (size_t i = 0; i < objectPtr->indexArray.size(); ++i)
		{
			std::cout << objectPtr->indexArray[i] << '\n';
		}
	}
}

const std::vector<string> & ObjLoader::getNameArray() const
{
	return cashNameArray;
}


const std::vector<float> & ObjLoader::getDataArray( const string & name, DataType type )
{
	const std::vector<float> * pointer;
	std::shared_ptr<Object> objectPtr = findObject(name);

	switch (type)
	{
	case DataType::vertex :
		pointer = &(objectPtr->vertexArray);
		break;
	case DataType::normal :
		pointer = &(objectPtr->normalArray);
		break;
	case DataType::texture :
		pointer = &(objectPtr->textureArray);
		break;
	}

	return *pointer;
}


const std::vector<unsigned int> & ObjLoader::getIndexArray( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->indexArray;
}

unsigned int ObjLoader::getEdgeSize( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->edgeSize;
}


bool ObjLoader::replaceData( const std::string & strData )
{
	const std::string::size_type length = strData.length();
	std::string::size_type startPos = 0, endPos;

	std::vector<float> globalVertexArray;
	std::vector<float> globalNormalArray;
	std::vector<float> globalTextureArray;

	std::vector<unsigned int> globalIndexVertexArray;
	std::vector<unsigned int> globalIndexNormalArray;
	std::vector<unsigned int> globalIndexTextureArray;

	unsigned int globalEdgeSize = 0;

	std::shared_ptr<Object> objectPtr (new Object);
	while ( (endPos = strData.find('\n', startPos)) != std::string::npos )
	{
		unsigned int lengthLine = endPos - startPos;
		if ('o' == strData[startPos])
		{
			++startPos;
			// if (objectPtr->name.size())
			// {
			// 	cashNameArray.push_back(objectPtr->name);
			// 	objectMap[objectPtr->name] = objectPtr;
			// 	objectPtr.reset(new Object);
			// }
			objectPtr->name = string(&strData[startPos], lengthLine);
		}
		else if ('v' == strData[startPos])
		{
			++startPos;
			if ('t' == strData[startPos])
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				globalTextureArray.push_back(std::strtof(start, &end));
				start = end;
				globalTextureArray.push_back(std::strtof(start, &end));
			}
			else if ('n' == strData[startPos])
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				float xn, yn, zn;
				xn = std::strtof(start, &end);
				start = end;
				yn = std::strtof(start, &end);
				start = end;
				zn = std::strtof(start, &end);

				float dividor = std::sqrt(xn*xn + yn*yn + zn*zn);
				xn /= dividor;
				yn /= dividor;
				zn /= dividor;

				globalNormalArray.push_back(xn);
				globalNormalArray.push_back(yn);
				globalNormalArray.push_back(zn);
			}
			else if ((' ' == strData[startPos]) || ('\t' == strData[startPos]))
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				float x, y, z;
				x = std::strtof(start, &end);
				start = end;
				y = std::strtof(start, &end);
				start = end;
				z = std::strtof(start, &end);

				globalVertexArray.push_back(x);
				globalVertexArray.push_back(y);
				globalVertexArray.push_back(z);
			}
		}
		else if ('f' == strData[startPos])
		{
			++startPos;
			std::string line = strData.substr(startPos, endPos);
			std::stringstream streamLine(line);
			unsigned int localEdgeSize = 0;

			unsigned int point;
			while (1)
			{
				streamLine >> point;
				if (!streamLine.fail()) {
					globalIndexVertexArray.push_back(point);
					++localEdgeSize;
				}
				else {
					if (globalEdgeSize == 0)
					{
						globalEdgeSize = localEdgeSize;
					}
					else if (globalEdgeSize != localEdgeSize)
					{
						std::cerr << "(globalEdgeSize != localEdgeSize)\n";
						return false;
					}
					break;
				}

				size_t pos = streamLine.tellg();
				streamLine.ignore(line.size() - pos - 1, '/');
				if ( streamLine.tellg() < (line.size() - 1) )
				{
					streamLine >> point;
					if (streamLine.good()) {
						globalIndexTextureArray.push_back(point);
					}
					else {
						streamLine.clear();
						streamLine.ignore(1);;
					}

					size_t pos = streamLine.tellg();
					streamLine.ignore(line.size() - pos - 1, '/');
					streamLine >> point;
					globalIndexNormalArray.push_back(point);
				}
				else {
					streamLine.seekg(pos);
				}
			}
		}
		startPos = endPos + 1;
	}

	size_t vs = globalVertexArray.size();
	size_t ns = globalNormalArray.size();
	size_t ts = globalTextureArray.size();

	size_t ivs = globalIndexVertexArray.size();
	size_t ins = globalIndexNormalArray.size();
	size_t its = globalIndexTextureArray.size();

	if (ins == 0)
	{
		globalIndexNormalArray.assign(ivs, 0);
		ins = ivs;
	}
	if (its == 0)
	{
		globalIndexTextureArray.assign(ivs, 0);
		its = ivs;
	}

	if ((ivs != ins) || (ivs != its) || (ivs == 0))
	{
		fprintf(stderr, "size problem: ivs = %lu ; its = %lu ; ins = %lu\n", ivs, its, ins);
		return false;
	}

	ObjLoader::IndexBox indexBox;
	std::set<ObjLoader::IndexBox> indexSet;
	objectPtr->edgeSize = globalEdgeSize;

	for (size_t i = 0; i < ivs; ++i)
	{
		indexBox.index = indexSet.size();
		indexBox.vertex = globalIndexVertexArray[i];
		indexBox.normal = globalIndexNormalArray[i];
		indexBox.texture = globalIndexTextureArray[i];

		auto iter = indexSet.find(indexBox);
		if (iter == indexSet.end())
		{
			indexSet.insert(indexBox);
			objectPtr->indexArray.push_back(indexBox.index);

			if ((vs < indexBox.vertex) || (ns < indexBox.normal) || (ts < indexBox.texture))
			{
				std::cerr << "index is too big\n";
				return false;
			}

			if (0 != indexBox.vertex) {
				size_t offset = 0;
				size_t base = (indexBox.vertex-1) * 3;
				objectPtr->vertexArray.push_back(globalVertexArray[base + (offset++)]);
				objectPtr->vertexArray.push_back(globalVertexArray[base + (offset++)]);
				objectPtr->vertexArray.push_back(globalVertexArray[base + (offset++)]);
			}
			if (0 != indexBox.normal) {
				size_t offset = 0;
				size_t base = (indexBox.normal-1) * 3;
				objectPtr->normalArray.push_back(globalNormalArray[base + (offset++)]);
				objectPtr->normalArray.push_back(globalNormalArray[base + (offset++)]);
				objectPtr->normalArray.push_back(globalNormalArray[base + (offset++)]);
			}
			if (0 != indexBox.texture) {
				size_t offset = 0;
				size_t base = (indexBox.texture-1) * 2;
				objectPtr->textureArray.push_back(globalTextureArray[base + (offset++)]);
				objectPtr->textureArray.push_back(globalTextureArray[base + (offset++)]);
			}
		}
		else
		{
			indexBox = *iter;
			objectPtr->indexArray.push_back(indexBox.index);
		}
	}

	cashNameArray.push_back(objectPtr->name);
	objectMap[objectPtr->name] = objectPtr;

	return true;
}



// ---------  --------- //


std::shared_ptr<ObjLoader::Object> ObjLoader::findObject( const string & name )
{
	std::shared_ptr<ObjLoader::Object> objectPtr;

	auto iter = objectMap.find(name);
	if ( iter == objectMap.end() )
	{
		cashNameArray.push_back(name);
		objectPtr.reset(new ObjLoader::Object(name));
		objectPtr->edgeSize = 0;
		objectMap[name] = objectPtr;
	}
	else
	{
		objectPtr = iter->second;
	}

	return objectPtr;
}



ObjLoader::Object::Object( ObjLoader::Object && other)
{
	std::swap(name, other.name);
	std::swap(edgeSize, other.edgeSize);
	std::swap(vertexArray, other.vertexArray);
	std::swap(normalArray, other.normalArray);
	std::swap(textureArray, other.textureArray);
	std::swap(indexArray, other.indexArray);
}


bool operator==( const ObjLoader::Object & first, const ObjLoader::Object & scnd )
{
	bool equal = (first.name == scnd.name) && (first.edgeSize == scnd.edgeSize);
	if (equal)
	{
		equal = first.vertexArray == scnd.vertexArray;
	}
	if (equal)
	{
		equal = first.normalArray == scnd.normalArray;
	}
	if (equal)
	{
		equal = first.textureArray == scnd.textureArray;
	}
	if (equal)
	{
		equal = first.indexArray == scnd.indexArray;
	}

	return equal;
}


bool operator<( const ObjLoader::Object & first, const ObjLoader::Object & scnd )
{
	return (first.name < scnd.name);
}

bool operator<( const ObjLoader::Object & first, const std::string & name )
{
	return (first.name < name);
}


bool operator<(const ObjLoader::IndexBox &first, const ObjLoader::IndexBox &scnd )
{
	bool less = true;
	if (!(first.vertex < scnd.vertex))
	{
		if (!(first.normal < scnd.normal))
		{
			if (!(first.texture < scnd.texture))
			{
				less = false;
			}
		}
	}
	return less;
}




///---------------------------//////////

int main()
{
	std::ifstream t("sphere.obj");
	std::string str;

	t.seekg(0, std::ios::end);
	str.reserve(t.tellg());
	t.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(t)),
	           std::istreambuf_iterator<char>());

	t.close();

	ObjLoader loader(str);
	return 0;
}
