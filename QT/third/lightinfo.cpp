
#include "lightinfo.h"


void LightInfo::setUniform( QOpenGLShaderProgram * shader, const QMatrix4x4 &viewMatrix ) const
{
	shader->setUniformValue("Light.Ld", diffuse);
	shader->setUniformValue("Light.La", ambient);
	shader->setUniformValue("Light.Ls", specular);
	shader->setUniformValue("Light.Position", viewMatrix * worldPosition);
}

