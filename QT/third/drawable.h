#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "points.h"
#include "material.h"
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QColor>
#include <QMatrix4x4>

class Drawable
{
public:
	Drawable( const Points *points, QOpenGLShaderProgram *shader );
	Drawable( const Points *points, QOpenGLShaderProgram *shader,
	          const MaterialInfo &mat, const QMatrix4x4 &modelMatrix );

	~Drawable();
	void draw( QOpenGLFunctions *func );

	void bindMaterial();
	void setMaterial( const MaterialInfo &mat );
	void setMaterial( const QColor *diffuse, const QColor *ambient, const QColor *specular,
	                  float shininess );
	const MaterialInfo & getMaterial();

	void bindModelMatrix();
	void setModelMatrix( const QMatrix4x4 & modelMatrix );
	const QMatrix4x4 & getModelMatrix();


private:
	void init();


private:
	MaterialInfo material;
	QMatrix4x4 modelMatrix;

	const Points *points;
	QOpenGLShaderProgram *shader;

	QOpenGLVertexArrayObject vao;
	QOpenGLBuffer pos_vbo;
	QOpenGLBuffer norm_vbo;
	QOpenGLBuffer tex_vbo;
	QOpenGLBuffer index_vbo;
};

#endif // DRAWABLE_H
