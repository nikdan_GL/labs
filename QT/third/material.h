#ifndef MATERIAL_H
#define MATERIAL_H

#include <QColor>
#include <QOpenGLShaderProgram>

class MaterialInfo
{
public:
	void setUniform( QOpenGLShaderProgram *shader ) const;

public:
	QColor diffuse;
	QColor ambient;
	QColor specular;
	QColor emition;
	float shininess;
};

#endif // MATERIAL_H
