#include "drawable.h"

Drawable::Drawable( const Points *points, QOpenGLShaderProgram *shader )
  : points(points), shader(shader),
    pos_vbo(QOpenGLBuffer::VertexBuffer),
    norm_vbo(QOpenGLBuffer::VertexBuffer),
    tex_vbo(QOpenGLBuffer::VertexBuffer),
    index_vbo(QOpenGLBuffer::IndexBuffer)
{
	if ( points && shader )
	{
		init();
	}
	else
	{
		this->points = nullptr;
		this->shader = nullptr;
	}
}


Drawable::Drawable( const Points *points, QOpenGLShaderProgram *shader,
                    const MaterialInfo &mat, const QMatrix4x4 &modelMatrix )
  : material(mat), modelMatrix(modelMatrix),
    points(points), shader(shader),
    pos_vbo(QOpenGLBuffer::VertexBuffer),
    norm_vbo(QOpenGLBuffer::VertexBuffer),
    tex_vbo(QOpenGLBuffer::VertexBuffer),
    index_vbo(QOpenGLBuffer::IndexBuffer)
{
	if ( this->points && this->shader )
	{
		init();
	}
	else
	{
		this->points = nullptr;
		this->shader = nullptr;
	}
}

void Drawable::draw( QOpenGLFunctions *func )
{
	if ( points && func )
	{
//		shader->bind();
		{
			bindMaterial();
			bindModelMatrix();

			vao.bind();
			{
				index_vbo.bind();
				GLenum mode = GL_TRIANGLES;
				if (points->getEdgeSize() == 4)
				{
					mode = GL_QUADS;
				}
				func->glDrawElements( mode, points->getIndex().size(), GL_UNSIGNED_INT, nullptr );
			}
			vao.release();
		}
//		shader->release();
	}
}


Drawable::~Drawable()
{
	if (points && shader)
	{
		pos_vbo.destroy();
		norm_vbo.destroy();
	}
}

void Drawable::bindMaterial()
{
	material.setUniform( shader );
}

void Drawable::setMaterial( const MaterialInfo &mat )
{
	if ( points )
	{
		material = mat;
	}
}

void Drawable::setMaterial( const QColor *diffuse, const QColor *ambient, const QColor *specular, float shininess )
{
	if ( points )
	{
		if ( diffuse )
		{
			material.diffuse = *diffuse;
		}
		if ( ambient )
		{
			material.ambient = *ambient;
		}
		if ( specular )
		{
			material.specular = *specular;
		}
		if ( shininess )
		{
			material.shininess = shininess;
		}
	}
}

const MaterialInfo & Drawable::getMaterial()
{
	return material;
}




void Drawable::setModelMatrix( const QMatrix4x4 & modelMatrix )
{
	if ( points )
	{
		this->modelMatrix = modelMatrix;
		bindModelMatrix();
	}
}

const QMatrix4x4 & Drawable::getModelMatrix()
{
	return modelMatrix;
}



////   private     /////

void Drawable::init()
{
	shader->bind();
	{
		vao.create();
		vao.bind();
		{
			pos_vbo.create();
			pos_vbo.bind();
			pos_vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
			pos_vbo.allocate(points->getVertex().data(), points->getVertex().size() * sizeof(float));
			shader->enableAttributeArray(0);
			shader->setAttributeBuffer(0, GL_FLOAT, 0, points->dimension);
			pos_vbo.release();

			if (points->getNormal().size())
			{
				norm_vbo.create();
				norm_vbo.bind();
				norm_vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
				norm_vbo.allocate(points->getNormal().data(), points->getNormal().size() * sizeof(float));
				shader->enableAttributeArray(1);
				shader->setAttributeBuffer(1, GL_FLOAT, 0, points->dimension);
				norm_vbo.release();
			}

			index_vbo.create();
			index_vbo.bind();
			index_vbo.allocate(points->getIndex().data(), points->getIndex().size() * sizeof(unsigned int));
			index_vbo.release();
		}
		vao.release();
	}
	shader->release();
}



void Drawable::bindModelMatrix()
{
	shader->setUniformValue("ModelMatrix", modelMatrix);
}
