#include "viewer.h"

const double Viewer::pi = 2*acos(0.0);

Viewer::Viewer() :
  distStep(1), angleStep(5)
{
	float distance(5);
	init(distance);
}

Viewer::Viewer( float distance ) :
  distStep(1), angleStep(5)
{
	init(distance);
}

Viewer::Viewer( float distStep, float angleStep, const QVector3D &xyzPosition ) :
  distStep(distStep), angleStep(angleStep)
{
	init( xyzPosition );
}


const QVector3D & Viewer::getXYZ() const
{
	return xyzPosition;
}


const QMatrix4x4 & Viewer::getViewMatrix() const
{
	return viewMatrix;
}



void Viewer::setXYZ( const QVector3D &position )
{
	xyzPosition = position;
	xyz2angle( &xyzPosition, &anglePosition );
	viewMatrix.setToIdentity();
	viewMatrix.lookAt( xyzPosition, QVector3D(0,0,0), QVector3D(0,1,0) );
}


bool Viewer::modify( unsigned char key )
{
	bool modified = true;

	switch (key) {
	case 'h': {
		anglePosition[1] += angleStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}
	case 'l': {
		anglePosition[1] -= angleStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}

	case 'j': {
		anglePosition[2] -= angleStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}
	case 'k': {
		anglePosition[2] += angleStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}

	case 'f': {
		anglePosition[0] -= distStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}
	case 'b': {
		anglePosition[0] += distStep;
		angle2xyz( &anglePosition, &xyzPosition );
		break;
	}
	default:
		modified = false;
	}

	if ( modified )
	{
		viewMatrix.setToIdentity();
		viewMatrix.lookAt( xyzPosition, QVector3D(0,0,0), QVector3D(0,1,0) );
	}

	return modified;
}





void Viewer::angle2xyz( const QVector3D *srcPosition, QVector3D *dstPosition )
{
	double localDistance = (*srcPosition)[0];
	double localAzimuth = deg2rad( (*srcPosition)[1] );
	double localAnglePlace = deg2rad( (*srcPosition)[2] );

	double y = sin(localAnglePlace) * localDistance;

	double x = sin(localAzimuth) * localDistance * cos(localAnglePlace);
	double z = cos(localAzimuth) * localDistance * cos(localAnglePlace);

	(*dstPosition)[0] = x;
	(*dstPosition)[1] = y;
	(*dstPosition)[2] = z;
}


void Viewer::xyz2angle( const QVector3D *srcPosition, QVector3D *dstPosition )
{
	(*dstPosition)[0] = getDistance( *srcPosition );
	(*dstPosition)[1] = getAzimuth( *srcPosition );
	(*dstPosition)[2] = getAnglePlace( *srcPosition );
}



double Viewer::getDistance( const QVector3D &srcPosition )
{
	return srcPosition.lengthSquared();
}


double Viewer::getAzimuth( const QVector3D &srcPosition )
{
	if ( srcPosition[2] == 0 )
	{
		return (srcPosition[0] < 0) ? -pi/2.0 : pi/2.0;
	}
	return atan( srcPosition[0] / srcPosition[2] );
}


double Viewer::getAnglePlace( const QVector3D &srcPosition )
{
	return asin( srcPosition[1] / getDistance(srcPosition) );
}


double Viewer::rad2deg( double rad )
{
	return rad * 180 / pi;
}

double Viewer::deg2rad( double deg )
{
	return deg / 180 * pi;
}




////   private   //////

void Viewer::init( float distance )
{
	anglePosition = QVector3D( distance, 0, 0 );
	angle2xyz( &anglePosition , &xyzPosition );
	viewMatrix.setToIdentity();
	viewMatrix.lookAt( xyzPosition, QVector3D(0,0,0), QVector3D(0,1,0) );
}

void Viewer::init( const QVector3D &srcPosition )
{
	xyzPosition = srcPosition;
	xyz2angle( &xyzPosition, &anglePosition );
	viewMatrix.setToIdentity();
	viewMatrix.lookAt( xyzPosition, QVector3D(0,0,0), QVector3D(0,1,0) );
}

