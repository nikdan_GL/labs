#ifndef LIGHTINFO_H
#define LIGHTINFO_H

#include <QColor>
#include <QVector4D>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>

class LightInfo
{
public:
	void setUniform( QOpenGLShaderProgram * shader, const QMatrix4x4 &viewMatrix ) const;

public:
	QVector4D worldPosition;

	QColor diffuse;
	QColor ambient;
	QColor specular;
};

#endif // LIGHTINFO_H

