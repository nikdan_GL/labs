#version 330

in vec4 LightIntensity;
layout( location = 0 ) out vec4 FragColor;

void main()
{
  FragColor = LightIntensity;
}
