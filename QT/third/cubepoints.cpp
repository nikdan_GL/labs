#include "cubepoints.h"

CubePoints::CubePoints( float sideScale ) :
  Points(72, 72, 0, 36)
{
	edgeSize = 3;
	float sideHalf = sideScale / 2.0f;

	vertex = std::vector<float>{
	  // Front
	  -sideHalf, -sideHalf, sideHalf,
	  sideHalf, -sideHalf, sideHalf,
	  sideHalf,  sideHalf, sideHalf,
	  -sideHalf,  sideHalf, sideHalf,
	  // Right
	  sideHalf, -sideHalf, sideHalf,
	  sideHalf, -sideHalf, -sideHalf,
	  sideHalf,  sideHalf, -sideHalf,
	  sideHalf,  sideHalf, sideHalf,
	  // Back
	  -sideHalf, -sideHalf, -sideHalf,
	  -sideHalf,  sideHalf, -sideHalf,
	  sideHalf,  sideHalf, -sideHalf,
	  sideHalf, -sideHalf, -sideHalf,
	  // Left
	  -sideHalf, -sideHalf, sideHalf,
	  -sideHalf,  sideHalf, sideHalf,
	  -sideHalf,  sideHalf, -sideHalf,
	  -sideHalf, -sideHalf, -sideHalf,
	  // Bottom
	  -sideHalf, -sideHalf, sideHalf,
	  -sideHalf, -sideHalf, -sideHalf,
	  sideHalf, -sideHalf, -sideHalf,
	  sideHalf, -sideHalf, sideHalf,
	  // Top
	  -sideHalf,  sideHalf, sideHalf,
	  sideHalf,  sideHalf, sideHalf,
	  sideHalf,  sideHalf, -sideHalf,
	  -sideHalf,  sideHalf, -sideHalf
  };

	normal = std::vector<float> {
	  // Front
	  0.0f, 0.0f, 1.0f,
	  0.0f, 0.0f, 1.0f,
	  0.0f, 0.0f, 1.0f,
	  0.0f, 0.0f, 1.0f,
	  // Right
	  1.0f, 0.0f, 0.0f,
	  1.0f, 0.0f, 0.0f,
	  1.0f, 0.0f, 0.0f,
	  1.0f, 0.0f, 0.0f,
	  // Back
	  0.0f, 0.0f, -1.0f,
	  0.0f, 0.0f, -1.0f,
	  0.0f, 0.0f, -1.0f,
	  0.0f, 0.0f, -1.0f,
	  // Left
	  -1.0f, 0.0f, 0.0f,
	  -1.0f, 0.0f, 0.0f,
	  -1.0f, 0.0f, 0.0f,
	  -1.0f, 0.0f, 0.0f,
	  // Bottom
	  0.0f, -1.0f, 0.0f,
	  0.0f, -1.0f, 0.0f,
	  0.0f, -1.0f, 0.0f,
	  0.0f, -1.0f, 0.0f,
	  // Top
	  0.0f, 1.0f, 0.0f,
	  0.0f, 1.0f, 0.0f,
	  0.0f, 1.0f, 0.0f,
	  0.0f, 1.0f, 0.0f
  };

	index = std::vector<unsigned int> {
	       0, 1, 2,  0, 2, 3,
	       4, 5, 6,  4, 6, 7,
	       8, 9,10,  8,10,11,
	      12,13,14, 12,14,15,
	      16,17,18, 16,18,19,
	      20,21,22, 20,22,23
  };
}
