
#include "material.h"

void MaterialInfo::setUniform( QOpenGLShaderProgram *shader ) const
{
	shader->setUniformValue("Material.Kd", diffuse);
	shader->setUniformValue("Material.Ka", ambient);
	shader->setUniformValue("Material.Ks", specular);
	shader->setUniformValue("Material.Ke", emition);
	shader->setUniformValue("Material.Shininess", shininess);
}

