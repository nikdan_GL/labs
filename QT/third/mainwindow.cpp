
#include "mainwindow.h"
#include "cubepoints.h"
#include "spherepoints.h"
#include "objpoints.h"
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QKeyEvent>
#include <iostream>




void MainWindow::initializeGL()
{
	func = new QOpenGLFunctions;
	func->initializeOpenGLFunctions();
	func->glEnable(GL_DEPTH_TEST);
	func->glEnable(GL_CULL_FACE);

//	glEnable(GL_BLEND);
//	glEnable(GL_DEPTH_TEST);


	// Set global information
	func->glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	{
		shader_program = new QOpenGLShaderProgram();

		vert_shader = new QOpenGLShader(QOpenGLShader::Vertex);
		vert_shader->compileSourceFile(":/shaders/vert.vert");

		QOpenGLShader * fragm_shader = new QOpenGLShader(QOpenGLShader::Fragment);
		fragm_shader->compileSourceFile(":/shaders/fragm.frag");
		// The program does not take ownership over the shaders, so store them in a vector so they can be deleted afterwards.
		shader_program->addShader(vert_shader);
		shader_program->addShader(fragm_shader);
		if (!shader_program->link()) {
			  qWarning("Failed to compile and link shader program");
				qWarning("Vertex shader log:");
				qWarning() << vert_shader->log();
				qWarning() << fragm_shader->log();
				qWarning("Shader program log:");
				qWarning() << shader_program->log();
		}
		fragm_shaders.push_back(fragm_shader);
		shader_program->link();

		{
			points = new CubePoints( 1.0 );
			points = new SpherePoints( 1.0 );
			points = new ObjPoints(":/objects/cone.obj");

			MaterialInfo mat;
			mat.diffuse.setHsvF (0.53, 0.7, 0.8);
			mat.ambient.setHsvF (0.53, 0.5, 0.3);
			mat.specular.setHsvF(0.53, 0.8, 1.0);
			mat.shininess = 100;

			QMatrix4x4 model;
//			model.rotate(-35, 1, 0, 0.1);
//			model.rotate(35, 0, 1, 0);

			figure3D.push_back( new Drawable( points, shader_program, mat, model ) );
		}

		angle = 15;
		projection.perspective(angle, 800.0/600.0, 1, 5000);

		viewer = Viewer(10.0);

		QVector4D lightPosition(0, 0, 3, 1);
		QColor white( Qt::white );
		lightInfo = new LightInfo;
		lightInfo->diffuse = white;
		lightInfo->ambient = white;
		lightInfo->specular = white;
		lightInfo->worldPosition = lightPosition;
		shader_program->bind();
		{
			lightInfo->setUniform( shader_program, viewer.getViewMatrix() );
		}
		shader_program->release();

		{
			points = new SpherePoints(0.1);

			MaterialInfo mat;
			mat.emition.setRgbF(1.0, 1.0, 1.0);
			mat.shininess = 1;

			QMatrix4x4 model;
			model.translate(lightPosition.toVector3D());

			figure3D.push_back( new Drawable( points, shader_program, mat, model ) );
		}
	}

	QOpenGLWindow::initializeGL();
}




void MainWindow::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
	projection.setToIdentity();
	projection.perspective(angle, (float)width / (float)height, 1, 5000);
	update();
	QOpenGLWindow::resizeGL(width, height);
}

void MainWindow::paintGL()
{
	// Clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shader_program->bind();
	for (size_t i = 0; i < figure3D.size(); ++i)
	{
		auto object = figure3D[ figure3D.size() - i - 1];
		setMatrices(object->getModelMatrix(), viewer.getViewMatrix());
		object->draw( func );
	}
	shader_program->release();
	QOpenGLWindow::paintGL();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	char key;
	if ( event->modifiers() == Qt::ShiftModifier )
	{
		key = toupper(event->key());
	}
	else
	{
		key = tolower(event->key());
	}
	bool modified = viewer.modify( key );
	switch (key) {
	case 'q':
		close();
		break;
	}

	if ( modified )
	{
		shader_program->bind();
		lightInfo->setUniform( shader_program, viewer.getViewMatrix() );
		shader_program->release();
		update();
	}
}


/////       private      ////


void MainWindow::setMatrices( const QMatrix4x4 &modelMatrix, const QMatrix4x4 &viewMatrix )
{
	QMatrix4x4 modelViewMatrix ( viewMatrix * modelMatrix );
	QMatrix3x3 normalMatrix ( modelViewMatrix.normalMatrix() );
	QMatrix4x4 mvp ( projection * modelViewMatrix );
//	shader_program->isLinked();
	{
		shader_program->setUniformValue("ModelViewMatrix", modelViewMatrix);
		shader_program->setUniformValue("NormalMatrix", normalMatrix);
		shader_program->setUniformValue("MVP", mvp);
	}
//	shader_program->release();
}
