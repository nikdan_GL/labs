#ifndef OBJPOINTS_H
#define OBJPOINTS_H

#include "points.h"
#include "loader.hpp"
//#include <string>


class ObjPoints : public Points
{
public:
	ObjPoints( const char * fileName );
	ObjPoints( std::string fileName );
	ObjPoints( const std::string & fileName );

private:
	void init( const char * fileName );

private:
	ObjLoader loader;
};

#endif // OBJPOINTS_H
