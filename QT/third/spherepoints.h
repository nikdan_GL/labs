#ifndef SPHEREPOINTS_H
#define SPHEREPOINTS_H

#include "points.h"

class SpherePoints : public Points
{
public:
	SpherePoints( float radius );

private:
	static const unsigned int slices = 100;
	static const unsigned int stacks = 100;

	float radius;
};

#endif // SPHEREPOINTS_H
