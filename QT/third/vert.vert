#version 330

layout (location = 0) in vec3 VertexPosition;
layout (location = 1) in vec3 VertexNormal;

out vec4 LightIntensity;

struct LightInfo {
  vec4 Position; // Light position in eye coords.
  vec4 La;       // Ambient light intensity
  vec4 Ld;       // Diffuse light intensity
  vec4 Ls;       // Specular light intensity
};
uniform LightInfo Light;

struct MaterialInfo {
  vec4 Ka;            // Ambient reflectivity
  vec4 Kd;            // Diffuse reflectivity
  vec4 Ks;            // Specular reflectivity
  vec4 Ke;            // Emition value
  float Shininess;    // Specular shininess factor
};
uniform MaterialInfo Material;


uniform mat4 ModelMatrix;
uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 MVP;


vec4 phongModel( vec4 position, vec3 norm );


void main()
{
  vec3 eyeNorm = normalize( NormalMatrix * VertexNormal );
  vec4 eyePosition = ModelViewMatrix * vec4(VertexPosition, 1.0);

  // Evaluate the lighting equation.
  LightIntensity = phongModel( eyePosition, eyeNorm );

  gl_Position = MVP * vec4(VertexPosition,1.0);
}



vec4 phongModel( vec4 position, vec3 norm )
{
  vec4 summary;
  float alphaValue;
  float maxValue;

  vec3 s = normalize(vec3(Light.Position - position));
  vec3 v = normalize(-position.xyz);
  vec3 r = reflect( -s, norm );
  float sDotN = max( dot(s,norm), 0.0 );
  float rDotN = max( dot(r,v), 0.0 );

  vec4 ambient = Light.La * Material.Ka;
  vec4 diffuse = Light.Ld * Material.Kd * sDotN;
  vec4 spec = vec4(0.0);
  if ( (0.0 < sDotN) && (0.0 < Material.Shininess) ) {
    spec = Light.Ls * Material.Ks * pow( rDotN, Material.Shininess );
  }

  summary = (ambient + diffuse + spec + Material.Ke);

	maxValue = max( summary.r, summary.g );
	maxValue = max( maxValue, summary.b );
	if ( 1.0 < maxValue ) {
		maxValue = 1 / maxValue;
		summary = summary * maxValue;
	}

	summary.a = 1.0;
	return summary;
}
