#include "points.h"


Points::Points( unsigned int vertexSize,
          unsigned int normalSize,
          unsigned int textureSize,
          unsigned int indexSize )
  : vertex(vertexSize)
  , normal(normalSize)
  , texture(textureSize)
  , index(indexSize)
  , edgeSize(3)
{
}

const std::vector<float> & Points::getVertex() const
{
	return vertex;
}
const std::vector<float> & Points::getNormal() const
{
	return normal;
}
const std::vector<float> & Points::getTexture() const
{
	return texture;
}
const std::vector<unsigned int> Points::getIndex() const
{
	return index;
}


unsigned int Points::getEdgeSize() const
{
	return edgeSize;
}
