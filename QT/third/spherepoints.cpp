
#include "spherepoints.h"
#include <cmath>

SpherePoints::SpherePoints( float radius )
  : Points(((slices+1)*(stacks+1)) * 3,
           ((slices+1)*(stacks+1)) * 3,
           0,
           (2*slices * (stacks-1) ) * 3 ),
    radius(radius)
{
	edgeSize = 3;
	const double pi = 2*std::acos(0.0);
	float thetaFac = 2*pi / slices;
	float phiFac = pi / stacks;

	unsigned int idx = 0;
	for( unsigned int i = 0; i <= slices; i++ )
	{
		float theta = i * thetaFac;
		for( unsigned int j = 0; j <= stacks; j++, idx += 3 )
		{
			float phi = j * phiFac;
			float nx = std::sin(phi) * std::cos(theta);
			float ny = std::sin(phi) * std::sin(theta);
			float nz = std::cos(phi);

			vertex[idx] = radius * nx;
			vertex[idx+1] = radius * ny;
			vertex[idx+2] = radius * nz;

			normal[idx] = nx;
			normal[idx+1] = ny;
			normal[idx+2] = nz;
		}
	}

	idx = 0;
	for( unsigned int i = 0; i < slices; i++ )
	{
		unsigned int stackStart = i * (stacks + 1);
		unsigned int nextStackStart = (i+1) * (stacks+1);
		for( unsigned int j = 0; j < stacks; j++ )
		{
			if( j == 0 )
			{
				index[idx] = stackStart;
				index[idx+1] = stackStart + 1;
				index[idx+2] = nextStackStart + 1;
				idx += 3;
			}
			else if( j == stacks - 1)
			{
				index[idx] = stackStart + j;
				index[idx+1] = stackStart + j + 1;
				index[idx+2] = nextStackStart + j;
				idx += 3;
			}
			else
			{
				index[idx] = stackStart + j;
				index[idx+1] = stackStart + j + 1;
				index[idx+2] = nextStackStart + j + 1;
				index[idx+3] = nextStackStart + j;
				index[idx+4] = stackStart + j;
				index[idx+5] = nextStackStart + j + 1;
				idx += 6;
			}
		}
	}
}
