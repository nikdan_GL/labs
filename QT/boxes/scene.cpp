/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "scene.h"

#include <QDebug>
#include <QtGui/qmatrix4x4.h>
#include <QtGui/qvector3d.h>
#include <cmath>

#include "3rdparty/fbm.h"

void checkGLErrors(const QString& prefix)
{
	switch (glGetError()) {
	case GL_NO_ERROR:
		//qDebug() << prefix << tr("No error.");
		break;
	case GL_INVALID_ENUM:
		qDebug() << prefix << QObject::tr("Invalid enum.");
		break;
	case GL_INVALID_VALUE:
		qDebug() << prefix << QObject::tr("Invalid value.");
		break;
	case GL_INVALID_OPERATION:
		qDebug() << prefix << QObject::tr("Invalid operation.");
		break;
	case GL_STACK_OVERFLOW:
		qDebug() << prefix << QObject::tr("Stack overflow.");
		break;
	case GL_STACK_UNDERFLOW:
		qDebug() << prefix << QObject::tr("Stack underflow.");
		break;
	case GL_OUT_OF_MEMORY:
		qDebug() << prefix << QObject::tr("Out of memory.");
		break;
	default:
		qDebug() << prefix << QObject::tr("Unknown error.");
		break;
	}
}


static void loadMatrix(const QMatrix4x4& m)
{
	// static to prevent glLoadMatrixf to fail on certain drivers
	static GLfloat mat[16];
	const float *data = m.constData();
	for (int index = 0; index < 16; ++index)
		mat[index] = data[index];
	glLoadMatrixf(mat);
}


//============================================================================//
//                             GraphicsWidget                                 //
//============================================================================//
class GraphicsWidget : public QGraphicsProxyWidget
{
public:
	  GraphicsWidget() : QGraphicsProxyWidget(0, Qt::Window) {}
protected:
		virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE
		{
			painter->setRenderHint(QPainter::Antialiasing, false);
			QGraphicsProxyWidget::paint(painter, option, widget);
			painter->setRenderHint(QPainter::Antialiasing, true);
		}
};




//============================================================================//
//                             RenderOptionsDialog                            //
//============================================================================//

RenderOptionsDialog::RenderOptionsDialog()
  : QDialog(0, Qt::CustomizeWindowHint | Qt::WindowTitleHint)
{
	setWindowOpacity(0.75);
	QGridLayout *layout = new QGridLayout;
	setLayout(layout);
	layout->setColumnStretch(1, 1);

	int row = 0;

	layout->addWidget(new QLabel(tr("Texture:")));
	m_textureCombo = new QComboBox;
	connect(m_textureCombo, SIGNAL(currentIndexChanged(int)), this, SIGNAL(textureChanged(int)));
	layout->addWidget(m_textureCombo);
	++row;

	layout->addWidget(new QLabel(tr("Shader:")));
	m_shaderCombo = new QComboBox;
	connect(m_shaderCombo, SIGNAL(currentIndexChanged(int)), this, SIGNAL(shaderChanged(int)));
	layout->addWidget(m_shaderCombo);
	++row;

	layout->setRowStretch(row, 1);
}

int RenderOptionsDialog::addTexture(const QString &name)
{
	m_textureCombo->addItem(name);
	return m_textureCombo->count() - 1;
}

int RenderOptionsDialog::addShader(const QString &name)
{
	m_shaderCombo->addItem(name);
	return m_shaderCombo->count() - 1;
}



//============================================================================//
//                                    Scene                                   //
//============================================================================//

const static char environmentShaderText[] =
    "uniform samplerCube env;"
    "void main() {"
    "gl_FragColor = textureCube(env, gl_TexCoord[1].xyz);"
    "}";

Scene::Scene(int width, int height, int maxTextureSize)
  : m_distExp(600)
  , m_maxTextureSize(maxTextureSize)
  , m_currentShaderId(0)
  , m_currentTexture(0)
  , m_box(0)
  , m_batman(0)
	, m_atoms(100)
  , m_vertexShader(0)
  , m_environmentShader(0)
  , m_environmentProgram(0)
{
	setSceneRect(0, 0, width, height);

	m_trackBalls[0] = TrackBall(0.05f, QVector3D(0, 1, 0), TrackBall::Sphere);
	m_trackBalls[1] = TrackBall(0.005f, QVector3D(0, 0, 1), TrackBall::Sphere);
	m_trackBalls[2] = TrackBall(0.0f, QVector3D(0, 1, 0), TrackBall::Plane);

	m_renderOptions = new RenderOptionsDialog;
	m_renderOptions->move(20, 120);
	m_renderOptions->resize(m_renderOptions->sizeHint());

	connect(m_renderOptions, SIGNAL(textureChanged(int)), this, SLOT(setTexture(int)));
	connect(m_renderOptions, SIGNAL(shaderChanged(int)), this, SLOT(setShader(int)));


	GraphicsWidget *proxy = new GraphicsWidget;
	proxy->setWidget(m_renderOptions);
	proxy->setCacheMode(QGraphicsItem::ItemCoordinateCache);
	proxy->setZValue(1e30); // Make sure the dialog is drawn on top of all other (OpenGL) items
	this->addItem(proxy);

	initGL();

	m_timer = new QTimer(this);
	m_timer->setInterval(50);
	connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
	m_timer->start();

	m_time.start();
}



Scene::~Scene()
{
	if (m_box)
		delete m_box;
	foreach (GLTexture *texture, m_textures)
		if (texture) delete texture;
	if (m_mainCubemap)
		delete m_mainCubemap;
	foreach (QGLShaderProgram *program, m_programs)
		if (program) delete program;
	if (m_vertexShader)
		delete m_vertexShader;
	foreach (QGLShader *shader, m_fragmentShaders)
		if (shader) delete shader;
	foreach (GLRenderTargetCube *rt, m_cubemaps)
		if (rt) delete rt;
	if (m_environmentShader)
		delete m_environmentShader;
	if (m_environmentProgram)
		delete m_environmentProgram;
}



void Scene::initGL()
{
	m_box = new GLRoundedBox(0.25f, 1.0f, 10);

	m_batman = new ObjBox(":/objects/batman.obj");
	m_phBatman = new Phisics(&m_batman->m_data, &m_batman->m_index);
	m_phBatman->m_radius = QVector3D(1.0, 1.0, 1.0);

	m_phFloor = new Phisics(&m_box->m_data, &m_box->m_index);
	m_phFloor->m_radius = QVector3D(1.0, 0.5, 1.0);
	m_phFloor->m_direction = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1),45);
	m_phFloor->m_model.translate(0, -1, 0);
	m_phFloor->m_model.rotate(m_phFloor->m_direction);
	m_phFloor->m_model.scale(m_phFloor->m_radius);

	m_vertexShader = new QGLShader(QGLShader::Vertex);
	m_vertexShader->compileSourceFile(QLatin1String(":/res/boxes/basic.vsh"));

	QStringList list;
	list << ":/res/boxes/cubemap_posx.jpg" << ":/res/boxes/cubemap_negx.jpg" << ":/res/boxes/cubemap_posy.jpg"
	     << ":/res/boxes/cubemap_negy.jpg" << ":/res/boxes/cubemap_posz.jpg" << ":/res/boxes/cubemap_negz.jpg";
	m_environment = new GLTextureCube(list, qMin(1024, m_maxTextureSize));
	m_environmentShader = new QGLShader(QGLShader::Fragment);
	m_environmentShader->compileSourceCode(environmentShaderText);
	m_environmentProgram = new QGLShaderProgram;
	m_environmentProgram->addShader(m_vertexShader);
	m_environmentProgram->addShader(m_environmentShader);
	m_environmentProgram->link();

	generateFbmNoise();

	m_mainCubemap = new GLRenderTargetCube(512);

	loadTextures();
	loadFragmShaders();

	loadColors();

	qsrand(QDateTime::currentMSecsSinceEpoch() / 1000);
	for (size_t i = 0; i < m_atoms.size(); ++i)
	{
		m_atoms[i] = new Phisics(&m_box->m_data, &m_box->m_index);
		m_atoms[i]->setStart(m_phBatman);
	}
}



void Scene::drawBackground(QPainter *painter, const QRectF &)
{
	float width = float(painter->device()->width());
	float height = float(painter->device()->height());

	painter->beginNativePainting();
	setStates();

	renderCubemaps();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	qgluPerspective(60.0, width / height, 0.01, 15000.0);

	glMatrixMode(GL_MODELVIEW);

	QMatrix4x4 view;
	view.rotate(m_trackBalls[2].rotation());
	view(2, 3) -= 2.0f * std::exp(m_distExp / 1200.0f);
	renderBoxes(view);

	defaultStates();

	for (size_t i = 0; i < m_atoms.size(); ++i)
	{
		if (50 < m_atoms[i]->m_centralPosition.lengthSquared()) {
			m_atoms[i]->setStart(m_phBatman);
		}
		m_atoms[i]->bummer(m_phFloor);
		for (size_t j = 0; j < m_phBoxArray.size(); ++j)
		{
			m_atoms[i]->bummer(m_phBoxArray[j]);
		}
		m_atoms[i]->update();
	}

	painter->endNativePainting();
}


// If one of the boxes should not be rendered, set excludeBox to its index.
// If the main box should not be rendered, set excludeBox to -1.
void Scene::renderBoxes(const QMatrix4x4 &view, int excludeBox)
{
	QMatrix4x4 invView = view.inverted();

	// If multi-texturing is supported, use three saplers.
	if (glActiveTexture) {
		glActiveTexture(GL_TEXTURE0);
		m_textures[m_currentTexture]->bind();
		glActiveTexture(GL_TEXTURE2);
		m_noise->bind();
		glActiveTexture(GL_TEXTURE1);
	} else {
		m_textures[m_currentTexture]->bind();
	}

	renderEnvironment(view);

	for (int i = 0; i < m_programs.size(); ++i)
	{
		if (i == excludeBox)
			continue;

		glPushMatrix();
		QQuaternion direction = m_trackBalls[1].rotation();
		direction *= QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 0.0f, 1.0f), 360.0f * i / m_programs.size());
		m_phBoxArray[i]->m_direction = direction;

		QMatrix4x4 m;
		m.rotate(direction);
		m.translate(2.0f, 0.0f, 0.0f);
		m.scale(m_phBoxArray[i]->m_radius);
		glMultMatrixf(m.constData());
		m_phBoxArray[i]->m_model = m;


		if (glActiveTexture) {
			if (m_cubemaps[i])
				m_cubemaps[i]->bind();
			else
				m_environment->bind();
		}
		m_programs[i]->bind();
		m_programs[i]->setUniformValue("tex", GLint(0));
		m_programs[i]->setUniformValue("env", GLint(1));
		m_programs[i]->setUniformValue("noise", GLint(2));
		m_programs[i]->setUniformValue("view", view);
		m_programs[i]->setUniformValue("invView", invView);
		m_box->draw();
		m_programs[i]->release();

		if (glActiveTexture) {
			if (m_cubemaps[i])
				m_cubemaps[i]->unbind();
			else
				m_environment->unbind();
		}
		glPopMatrix();
	}

	if (-1 != excludeBox)
	{
		glPushMatrix();
		QMatrix4x4 m;
		m.rotate(m_trackBalls[0].rotation());
		glMultMatrixf(m.constData());
		m_phBatman->m_model = m;

		if (glActiveTexture) {
			m_mainCubemap->bind();
		}

		m_programs[m_currentShaderId]->bind();
		m_programs[m_currentShaderId]->setUniformValue("tex", GLint(0));
		m_programs[m_currentShaderId]->setUniformValue("env", GLint(1));
		m_programs[m_currentShaderId]->setUniformValue("noise", GLint(2));
		m_programs[m_currentShaderId]->setUniformValue("view", view);
		m_programs[m_currentShaderId]->setUniformValue("invView", invView);
		m_batman->draw();
		m_programs[m_currentShaderId]->release();

		if (glActiveTexture) {
			m_mainCubemap->unbind();
		}
		glPopMatrix();
	}

	{
		glPushMatrix();
		glMultMatrixf(m_phFloor->m_model.constData());

		m_environment->bind();

		m_programs[3]->bind();
		m_programs[3]->setUniformValue("tex", GLint(0));
		m_programs[3]->setUniformValue("env", GLint(1));
		m_programs[3]->setUniformValue("noise", GLint(2));
		m_programs[3]->setUniformValue("view", view);
		m_programs[3]->setUniformValue("invView", invView);
		m_box->draw();
		m_programs[3]->release();

		m_environment->unbind();
		glPopMatrix();
	}


	for (size_t i = 0; i < m_atoms.size(); ++i)
	{
		if (50 < m_atoms[i]->m_centralPosition.lengthSquared()) {
			continue;
		}

		glPushMatrix();
		glMultMatrixf(m_atoms[i]->m_model.constData());

		m_environment->bind();

		m_programs[3]->bind();
		m_programs[3]->setUniformValue("tex", GLint(0));
		m_programs[3]->setUniformValue("env", GLint(1));
		m_programs[3]->setUniformValue("noise", GLint(2));
		m_programs[3]->setUniformValue("view", view);
		m_programs[3]->setUniformValue("invView", invView);
		m_box->draw();
		m_programs[3]->release();

		m_environment->unbind();
		glPopMatrix();
	}

	if (glActiveTexture) {
		glActiveTexture(GL_TEXTURE2);
		m_noise->unbind();
		glActiveTexture(GL_TEXTURE0);
	}
	m_textures[m_currentTexture]->unbind();
}


void Scene::renderEnvironment(const QMatrix4x4 &view)
{
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	QMatrix4x4 viewRotation(view);
	viewRotation(3, 0) = viewRotation(3, 1) = viewRotation(3, 2) = 0.0f;
	viewRotation(0, 3) = viewRotation(1, 3) = viewRotation(2, 3) = 0.0f;
	viewRotation(3, 3) = 1.0f;
	loadMatrix(viewRotation);
	glScalef(50.0f, 50.0f, 50.0f);

	// Don't render the environment if the environment texture can't be set for the correct sampler.
	if (glActiveTexture)
	{
		m_environment->bind();
		m_environmentProgram->bind();
		m_environmentProgram->setUniformValue("tex", GLint(0));
		m_environmentProgram->setUniformValue("env", GLint(1));
		m_environmentProgram->setUniformValue("noise", GLint(2));
		m_box->draw();
		m_environmentProgram->release();
		m_environment->unbind();
	}

	loadMatrix(view);

	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
}


void Scene::setStates()
{
	//glClearColor(0.25f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	//glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	setLights();

	float materialSpecular[] = {0.5f, 0.5f, 0.5f, 1.0f};
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 32.0f);
}

void Scene::setLights()
{
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	//float lightColour[] = {1.0f, 1.0f, 1.0f, 1.0f};
	float lightDir[] = {0.0f, 0.3f, 1.0f, 0.0f};
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColour);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, lightColour);
	glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, 1.0f);
	glEnable(GL_LIGHT0);
}

void Scene::defaultStates()
{
	//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	//glDisable(GL_COLOR_MATERIAL);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHT0);
	glDisable(GL_NORMALIZE);

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, 0.0f);
	float defaultMaterialSpecular[] = {0.0f, 0.0f, 0.0f, 1.0f};
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, defaultMaterialSpecular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);
}


void Scene::renderCubemaps()
{
	QMatrix4x4 mat;
	GLRenderTargetCube::getProjectionMatrix(mat, 0.1f, 1000.0f);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	loadMatrix(mat);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	QVector3D center;

	for (int i = 0; i < m_cubemaps.size(); ++i)
	{
		if (0 == m_cubemaps[i])
			continue;

		float angle = 2.0f * PI * i / m_cubemaps.size();

		center = m_trackBalls[1].rotation().rotatedVector(QVector3D(std::cos(angle), std::sin(angle), 0.0f));

		for (int face = 0; face < 6; ++face)
		{
			m_cubemaps[i]->begin(face);

			GLRenderTargetCube::getViewMatrix(mat, face);
			QVector4D v = QVector4D(-center.x(), -center.y(), -center.z(), 1.0);
			mat.setColumn(3, mat * v);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			renderBoxes(mat, i);

			m_cubemaps[i]->end();
		}
	}

	for (int face = 0; face < 6; ++face)
	{
		m_mainCubemap->begin(face);
		GLRenderTargetCube::getViewMatrix(mat, face);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		renderBoxes(mat, -1);

		m_mainCubemap->end();
	}

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}




//============================================================================//
//                             user event methods                             //
//============================================================================//


QPointF Scene::pixelPosToViewPos(const QPointF& p)
{
	return QPointF(2.0 * float(p.x()) / width() - 1.0,
	               1.0 - 2.0 * float(p.y()) / height());
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseMoveEvent(event);
	if (event->isAccepted())
		return;

	if (event->buttons() & Qt::LeftButton) {
		m_trackBalls[0].move(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	} else {
		m_trackBalls[0].release(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
	}

	if (event->buttons() & Qt::RightButton) {
		m_trackBalls[1].move(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	} else {
		m_trackBalls[1].release(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
	}

	if (event->buttons() & Qt::MidButton) {
		m_trackBalls[2].move(pixelPosToViewPos(event->scenePos()), QQuaternion());
		event->accept();
	} else {
		m_trackBalls[2].release(pixelPosToViewPos(event->scenePos()), QQuaternion());
	}
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mousePressEvent(event);
	if (event->isAccepted())
		return;

	if (event->buttons() & Qt::LeftButton) {
		m_trackBalls[0].push(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	}

	if (event->buttons() & Qt::RightButton) {
		m_trackBalls[1].push(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	}

	if (event->buttons() & Qt::MidButton) {
		m_trackBalls[2].push(pixelPosToViewPos(event->scenePos()), QQuaternion());
		event->accept();
	}
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsScene::mouseReleaseEvent(event);
	if (event->isAccepted())
		return;

	if (event->button() == Qt::LeftButton) {
		m_trackBalls[0].release(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	}

	if (event->button() == Qt::RightButton) {
		m_trackBalls[1].release(pixelPosToViewPos(event->scenePos()), m_trackBalls[2].rotation().conjugate());
		event->accept();
	}

	if (event->button() == Qt::MidButton) {
		m_trackBalls[2].release(pixelPosToViewPos(event->scenePos()), QQuaternion());
		event->accept();
	}
}

void Scene::wheelEvent(QGraphicsSceneWheelEvent * event)
{
	QGraphicsScene::wheelEvent(event);
	if (!event->isAccepted())
	{
		m_distExp += event->delta();
		if (m_distExp < -8 * 120)
			m_distExp = -8 * 120;
		if (m_distExp > 20 * 120)
			m_distExp = 20 * 120;
		event->accept();
	}
}

void Scene::setShader(int index)
{
	if (index >= 0 && index < m_fragmentShaders.size())
	{
		m_currentShaderId = index;
	}
}

void Scene::setTexture(int index)
{
	if (index >= 0 && index < m_textures.size())
	{
		m_currentTexture = index;
	}
}

void Scene::setColorParameter(const QString &name, QRgb color)
{
	// set the color in all programs
	foreach (QGLShaderProgram *program, m_programs)
	{
		program->bind();
		program->setUniformValue(program->uniformLocation(name), QColor(color));
		program->release();
	}
}




//============================================================================//
//                             private methods                                //
//============================================================================//

void Scene::generateFbmNoise()
{
	const int NOISE_SIZE = 128; // for a different size, B and BM in fbm.c must also be changed
	m_noise = new GLTexture3D(NOISE_SIZE, NOISE_SIZE, NOISE_SIZE);
	QRgb *data = new QRgb[NOISE_SIZE * NOISE_SIZE * NOISE_SIZE];
	memset(data, 0, NOISE_SIZE * NOISE_SIZE * NOISE_SIZE * sizeof(QRgb));
	QRgb *p = data;
	float pos[3];
	for (int k = 0; k < NOISE_SIZE; ++k) {
		pos[2] = k * (0x20 / (float)NOISE_SIZE);
		for (int j = 0; j < NOISE_SIZE; ++j) {
			for (int i = 0; i < NOISE_SIZE; ++i) {
				for (int byte = 0; byte < 4; ++byte) {
					pos[0] = (i + (byte & 1) * 16) * (0x20 / (float)NOISE_SIZE);
					pos[1] = (j + (byte & 2) * 8) * (0x20 / (float)NOISE_SIZE);
					*p |= (int)(128.0f * (noise3(pos) + 1.0f)) << (byte * 8);
				}
				++p;
			}
		}
	}
	m_noise->load(NOISE_SIZE, NOISE_SIZE, NOISE_SIZE, data);
	delete[] data;
}


void Scene::loadTextures()
{
	// Load all .png files as textures
	m_currentTexture = 0;
	QStringList filter = QStringList("*.png");
	QList<QFileInfo> files = QDir(":/res/boxes/").entryInfoList(filter, QDir::Files | QDir::Readable);

	foreach (QFileInfo file, files)
	{
		GLTexture *texture = new GLTexture2D(file.absoluteFilePath(), qMin(256, m_maxTextureSize), qMin(256, m_maxTextureSize));
		if (texture->failed())
		{
			delete texture;
			continue;
		}
		m_textures << texture;
		m_renderOptions->addTexture(file.baseName());
	}

	if (m_textures.size() == 0)
	{
		m_textures << new GLTexture2D(qMin(64, m_maxTextureSize), qMin(64, m_maxTextureSize));
	}
}


void Scene::loadFragmShaders()
{
	// Load all .fsh files as fragment shaders
	m_currentShaderId = 0;
	QStringList filter = QStringList("*.fsh");
	QList<QFileInfo> files = QDir(":/res/boxes/").entryInfoList(filter, QDir::Files | QDir::Readable);
	foreach (QFileInfo file, files)
	{
		QGLShaderProgram *program = new QGLShaderProgram;
		QGLShader* shader = new QGLShader(QGLShader::Fragment);
		shader->compileSourceFile(file.absoluteFilePath());
		// The program does not take ownership over the shaders, so store them in a vector so they can be deleted afterwards.
		program->addShader(m_vertexShader);
		program->addShader(shader);
		if (!program->link())
		{
			qWarning("Failed to compile and link shader program");
			qWarning("Vertex shader log:");
			qWarning() << m_vertexShader->log();
			qWarning() << "Fragment shader log ( file =" << file.absoluteFilePath() << "):";
			qWarning() << shader->log();
			qWarning("Shader program log:");
			qWarning() << program->log();

			delete shader;
			delete program;
			continue;
		}

		m_fragmentShaders << shader;
		m_programs << program;
		m_renderOptions->addShader(file.baseName());

		m_phBoxArray.push_back(new Phisics(&m_box->m_data, &m_box->m_index));
		m_phBoxArray.back()->m_radius = QVector3D(0.2, 0.6, 0.6);

		program->bind();
		m_cubemaps << ((program->uniformLocation("env") != -1) ? new GLRenderTargetCube(qMin(256, m_maxTextureSize)) : 0);
		program->release();
	}

	if (m_programs.size() == 0) {
		m_programs << new QGLShaderProgram;
	}
}


void Scene::loadColors()
{
	QStringList filter = QStringList("*.par");
	QList<QFileInfo> files = QDir(":/res/boxes/").entryInfoList(filter, QDir::Files | QDir::Readable);

	size_t counter_name = 0;
	foreach (QFileInfo fileInfo, files)
	{
		QFile file(fileInfo.absoluteFilePath());
		if (file.open(QIODevice::ReadOnly))
		{
			while (!file.atEnd())
			{
				QList<QByteArray> tokens = file.readLine().simplified().split(' ');
				QList<QByteArray>::const_iterator it = tokens.begin();
				if ((it == tokens.end()) || (++it == tokens.end()))
				{
					continue;
				}
				QByteArray name = *it;
				bool singleElement = (tokens.size() == 3); // type, name and one value
				char counter[10] = "000000000";
				int counterPos = 8; // position of last digit
				while (++it != tokens.end()) {
					++counter_name;
					if (!singleElement)
					{
						name += '[';
						name += counter + counterPos;
						name += ']';
						int j = 8; // position of last digit
						++counter[j];
						while (j > 0 && counter[j] > '9')
						{
							counter[j] = '0';
							++counter[--j];
						}
						if (j < counterPos)
						{
							counterPos = j;
						}
					}

					bool ok;
					setColorParameter(name, it->toUInt(&ok, 16));
				}
			}
			file.close();
		}
	}
}

