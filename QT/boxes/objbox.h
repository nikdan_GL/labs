#ifndef OBJBOX_H
#define OBJBOX_H


#include <QtWidgets>
#include <QtOpenGL>

#include "gltrianglemesh.h"
#include "glbuffers.h"
#include "p3t2n3vertex.h"
#include "objloader.h"


class ObjBox : public GLTriangleMesh<P3T2N3Vertex, unsigned short>
{
public:
	ObjBox( const char * fileName );
	ObjBox( std::string fileName );
	ObjBox( const std::string & fileName );

private:
	void initLoader( const char * fileName );

private:
	ObjLoader loader;
};


#endif // OBJBOX_H
