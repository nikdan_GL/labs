
#include "objbox.h"
#include <string>
#include <fstream>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDebug>


ObjBox::ObjBox( const char * fileName )
{
	initLoader(fileName);
}

ObjBox::ObjBox( std::string fileName )
{
	initLoader(fileName.c_str());
}

ObjBox::ObjBox( const std::string & fileName)
{
	initLoader(fileName.c_str());
}



void ObjBox::initLoader( const char * fileName )
{
	QString qstrFileName(fileName);
	QFile mFile(qstrFileName);

	if(!mFile.open(QFile::ReadOnly | QFile::Text)){
		qDebug() << "could not open file for read";
		return;
	}

	QTextStream in(&mFile);
	QString text = in.readAll();
	mFile.close();

	if (loader.replaceData(text.toStdString()))
	{
		std::string name = *(loader.getNameArray().begin());
		const std::vector<QVector3D> & vertexArray = loader.getVertexArray(name);
		const std::vector<QVector3D> & normalArray = loader.getNormalArray(name);
		const std::vector<QVector2D> & textureArray = loader.getTextureArray(name);
		const std::vector<unsigned int> indexArray = loader.getIndexArray(name);

		m_vb.init(vertexArray.size());
		m_ib.init(indexArray.size());
		m_data.resize(vertexArray.size());
		m_index.resize(indexArray.size());

		P3T2N3Vertex *vp = m_vb.lock();
		unsigned short *ip = m_ib.lock();

		if (!vp || !ip) {
			qWarning("GLRoundedBox::GLRoundedBox: Failed to lock vertex buffer and/or index buffer.");
			m_ib.unlock();
			m_vb.unlock();
			return;
		}

		for (size_t iv = 0; iv < vertexArray.size(); ++iv)
		{
			vp[iv].position = vertexArray[iv];
			vp[iv].normal = normalArray[iv];
			m_data[iv] = vp[iv];
		}

		for (size_t it = 0; it < textureArray.size(); ++it)
		{
			vp[it].texCoord = textureArray[it];
		}

		for (size_t ii = 0; ii < indexArray.size(); ++ii)
		{
			ip[ii] = indexArray[ii];
			m_index[ii] = indexArray[ii];
		}

		m_ib.unlock();
		m_vb.unlock();
	}
}
