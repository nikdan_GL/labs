
#include "phisics.h"

#include <set>
#include <cmath>

#include <QDebug>


Phisics::Phisics( const std::vector<P3T2N3Vertex> * data,
                  const std::vector<unsigned short> * index )
  : m_data(data)
  , m_index(index)
{
	m_time = 0;
	epoh = 1;

	float x = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	float y = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	float z = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	float angle = (static_cast<float>(qrand()) / RAND_MAX) * 360;
	m_direction = QQuaternion::fromAxisAndAngle(x, y, z, angle);
}


void Phisics::setStart(const Phisics * starter)
{
	if (!m_data || !starter || (!starter->m_data)) {
		return;
	}

	m_time = 0;

	float delimator = std::atan(static_cast<float>(epoh++) / 5) * 10;
	float radius = static_cast<double>(qrand()) / RAND_MAX / delimator;
	m_radius = QVector3D(radius, radius, radius);
	m_mass = radius * radius * radius;

	if (20 <= epoh) {
		m_radius = QVector3D(0.01, 0.01, 0.01);
		m_data = nullptr;
	}


	float x = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	float y = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	float z = (static_cast<float>(qrand()) / RAND_MAX) - 0.5;
	QVector3D normal(x, y, z);
	normal.normalize();

	QMatrix4x4 m = starter->m_model;

	double maxCorr = -1;
	size_t maxIndex = 0;
	for (size_t i = 0; i < starter->m_data->size(); ++i)
	{
		double corr = QVector3D::dotProduct(normal, (*starter->m_data)[i].normal);
		if ( maxCorr < corr)
		{
			maxCorr = corr;
			maxIndex = i;
		}
	}

	m_centralPosition = m * (*starter->m_data)[maxIndex].position;
	m_velocity = QMatrix4x4(m.normalMatrix()) * (*starter->m_data)[maxIndex].normal * 2 * 0.1;

	m_model.setToIdentity();
	m_model.translate(m_centralPosition);
	m_model.rotate(m_direction);
	m_model.scale(m_radius);
}


void Phisics::update()
{
	if (!m_data) {
		return;
	}

	m_time += 0.0001;
	m_centralPosition += (m_velocity * m_time) + (m_force * m_time * m_time);
	m_velocity += (m_force / m_mass) * m_time;

	m_force =
			QVector3D::crossProduct( m_centralPosition , QVector3D(0, 1, 0)) * 0.05
//	    + m_centralPosition / m_centralPosition.lengthSquared() * 0.01
//			-	m_velocity * m_radius * m_radius * 10
			+ QVector3D(0, -1, 0) * m_mass * 200;

	m_model.setToIdentity();
	m_model.translate(m_centralPosition);
	m_model.rotate(m_direction);
	m_model.scale(m_radius);
}

void Phisics::bummer( const Phisics * other )
{
	if (!m_data || !other || !(other->m_data)) {
		return;
	}


	QVector3D otherCentral = other->m_model * QVector3D(0,0,0);
	QVector3D distance = m_centralPosition - otherCentral;
	if ((distance.length() < ((other->m_radius + m_radius).length())/3) &&
	    (QVector3D::dotProduct(distance, m_velocity) < 0))
	{
//		const QMatrix4x4 & modelMatrixOther = other->m_model;
//		QMatrix4x4 normalMatrixOther( modelMatrixOther.normalMatrix() );

//		std::set<unsigned short> candidateIndexSet;
//		for (unsigned short i = 0; i < other->m_data->size(); ++i)
//		{
//			QVector3D normal (normalMatrixOther * other->m_data->at(i).normal);
//			if (QVector3D::dotProduct(m_velocity, normal) < 0)
//			{
//				QVector3D position = (modelMatrixOther * other->m_data->at(i).position);

//				if ((position - m_centralPosition).length() < (other->m_radius + m_radius).length())
//				{
//					candidateIndexSet.insert(i);
//				}
//			}
//		}

		QVector3D xNorm = other->m_direction.rotatedVector(QVector3D(other->m_radius.x(), 0, 0));
		QVector3D yNorm = other->m_direction.rotatedVector(QVector3D(0, other->m_radius.y(), 0));
		QVector3D zNorm = other->m_direction.rotatedVector(QVector3D(0, 0, other->m_radius.z()));

		QVector3D xNormDir = xNorm;
		QVector3D yNormDir = yNorm;
		QVector3D zNormDir = zNorm;

		xNormDir.normalize();
		yNormDir.normalize();
		zNormDir.normalize();

		float xDist = m_centralPosition.distanceToPlane(otherCentral + xNorm, xNormDir);
		float yDist = m_centralPosition.distanceToPlane(otherCentral + yNorm, yNormDir);
		float zDist = m_centralPosition.distanceToPlane(otherCentral + zNorm, zNormDir);

		float invXDist = -xDist - 2*xNorm.length();
		float invYDist = -yDist - 2*yNorm.length();
		float invZDist = -zDist - 2*zNorm.length();

		if ((xDist < m_radius.x()) && (yDist < m_radius.y()) && (zDist < m_radius.z()))
		{
			if ((invXDist < m_radius.x()) && (invYDist < m_radius.y()) && (invZDist < m_radius.z()))
			{
				QVector3D planeNormal;

				if (std::abs(xDist) < m_radius.x())
				{
					planeNormal = xNormDir;
				}
				else if (std::abs(invXDist) < m_radius.x())
				{
					planeNormal = -xNormDir;
				}
				else if (std::abs(yDist) < m_radius.y())
				{
					planeNormal = yNormDir;
				}
				else if (std::abs(invYDist) < m_radius.y())
				{
					planeNormal = -yNormDir;
				}
				else if (std::abs(zDist) < m_radius.z())
				{
					planeNormal = zNormDir;
				}
				else //if (std::abs(invZDist) < m_radius.z())
				{
					planeNormal = -zNormDir;
				}
//				else
//				{
//					return;
//				}

				bum(planeNormal);
			}
		}

//		for (size_t i = 0; i < other->m_index->size(); i += 3)
//		{
//			unsigned short firstIndex = other->m_index->at(i);
//			unsigned short secndIndex = other->m_index->at(i+1);
//			unsigned short thirdIndex = other->m_index->at(i+2);

//			size_t count = candidateIndexSet.count(firstIndex)
//			    + candidateIndexSet.count(secndIndex)
//			    + candidateIndexSet.count(thirdIndex);

//			if (0 < count)
//			{
//				QVector3D planeNormal = other->m_data->at(firstIndex).normal
//				    + other->m_data->at(secndIndex).normal
//				    + other->m_data->at(thirdIndex).normal;
//				planeNormal = normalMatrixOther * planeNormal;
//				planeNormal.normalize();

//				QVector3D firstPosition = other->m_data->at(firstIndex).position;
//				QVector3D secndPosition = other->m_data->at(secndIndex).position;
//				QVector3D thirdPosition = other->m_data->at(thirdIndex).position;
//				QVector3D planePosition = (firstPosition + secndPosition + thirdPosition) / 3.0f;
//				planePosition = modelMatrixOther * planePosition;


//				float distanceToPlane = m_centralPosition.distanceToPlane(planePosition, planeNormal);

//				if ((0 < distanceToPlane) && (distanceToPlane < m_radius.length()))
//				{
//					if ((planePosition - m_centralPosition).length() < m_radius.length())
//					{
//						bum(planeNormal);
//						break;
//					}

//					float firstDot = QVector3D::dotProduct(thirdPosition - firstPosition,
//					                                       thirdPosition - m_centralPosition);
//					firstDot /= (thirdPosition - firstPosition).length();
//					firstDot /= (thirdPosition - m_centralPosition).length();

//					float secndDot = QVector3D::dotProduct(thirdPosition - secndPosition,
//					                                       thirdPosition - m_centralPosition);
//					secndDot /= (thirdPosition - secndPosition).length();
//					secndDot /= (thirdPosition - m_centralPosition).length();

//					float thirdDot = QVector3D::dotProduct(secndPosition - thirdPosition,
//					                                       secndPosition - m_centralPosition);
//					thirdDot /= (secndPosition - thirdPosition).length();
//					thirdDot /= (secndPosition - m_centralPosition).length();


//					if ((0.5 < firstDot) && (0.5 < secndDot) && (0.5 < thirdDot))
//					{
////						qDebug() << firstDot << "  " << secndDot << "  " << thirdDot;
//						bum(planeNormal);
//						break;
//					}
//				}
//			}
//		}

	}

}


void Phisics::bum(const QVector3D &normal)
{
	QVector3D rotationAxis = QVector3D::crossProduct(normal, m_velocity);
	QMatrix4x4 matrixRotate;
	matrixRotate.rotate(-90, rotationAxis);
	m_velocity = matrixRotate * m_velocity;
}
