#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <vector>
#include <string>
#include <utility>
#include <map>
#include <set>
#include <memory>

#include <QVector3D>
#include <QVector2D>


class ObjLoader
{
public:
	ObjLoader() = default;
	ObjLoader( const ObjLoader & other ) = delete;
	ObjLoader& operator=( const ObjLoader & other ) = delete;

	ObjLoader( const std::string & strData );
	bool replaceData( const std::string & strData );

	enum class Type { name, data, index };
	enum class DataType { vertex, normal, texture };

	const std::vector<std::string> & getNameArray() const;
	const std::vector<QVector3D> & getVertexArray( const std::string & name );
	const std::vector<QVector3D> & getNormalArray( const std::string & name );
	const std::vector<QVector2D> & getTextureArray( const std::string & name );
	const std::vector<unsigned int> & getIndexArray( const std::string & name );
	unsigned int getEdgeSize( const std::string & name );

	struct Object
	{
		std::string name;
		unsigned int edgeSize;
		std::vector<QVector3D> vertexArray;
		std::vector<QVector3D> normalArray;
		std::vector<QVector2D> textureArray;
		std::vector<unsigned int> indexArray;

		Object() = default;
		Object( ObjLoader::Object && other);
		Object( const std::string & name) : name(name) {}
		Object( const ObjLoader::Object & other ) = delete;
		ObjLoader::Object & operator=( const ObjLoader::Object & other ) = delete;

		void normalize();

		friend bool operator==(const ObjLoader::Object & first, const ObjLoader::Object & scnd);
		friend bool operator< (const ObjLoader::Object & first, const ObjLoader::Object & scnd);
		friend bool operator< (const ObjLoader::Object & first, const std::string & name);
	};

	struct IndexBox
	{
		unsigned int vertex;
		unsigned int normal;
		unsigned int texture;

		unsigned int index;

		friend bool operator< (const ObjLoader::IndexBox & first, const ObjLoader::IndexBox &scnd);
	};


private:


	std::shared_ptr<ObjLoader::Object> findObject( const std::string & name );


	std::map<std::string, std::shared_ptr<ObjLoader::Object> > objectMap;
	std::vector<std::string> cashNameArray;
};


#endif // OBJLOADER_H
