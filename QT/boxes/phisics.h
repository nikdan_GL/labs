#ifndef PHISICS_H
#define PHISICS_H

#include "p3t2n3vertex.h"
#include <vector>

#include <QTime>
#include <QVector3D>
#include <QQuaternion>
#include <QMatrix4x4>

class Phisics
{
public:
	Phisics( const std::vector<P3T2N3Vertex> * data,
	         const std::vector<unsigned short> *index );

	void setStart( const Phisics * starter );
	void update();
	void bummer( const Phisics * other );
	void bum( const QVector3D & normal);

	double m_time;
	QVector3D m_radius;
	double m_mass;

	QVector3D m_centralPosition;
	QQuaternion m_direction;
	QVector3D m_velocity;
	QVector3D m_force;

	QMatrix4x4 m_model;

	size_t epoh;

	const std::vector<P3T2N3Vertex> * m_data;
	const std::vector<unsigned short> * m_index;
};

#endif // PHISICS_H
