
#include "objloader.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>

#include <QDebug>

using std::string;


ObjLoader::ObjLoader( const std::string & strData )
{
	replaceData( strData );
}

const std::vector<string> & ObjLoader::getNameArray() const
{
	return cashNameArray;
}


const std::vector<QVector3D> & ObjLoader::getVertexArray( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->vertexArray;
}

const std::vector<QVector3D> & ObjLoader::getNormalArray( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->normalArray;
}

const std::vector<QVector2D> & ObjLoader::getTextureArray( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->textureArray;
}


const std::vector<unsigned int> & ObjLoader::getIndexArray( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->indexArray;
}

unsigned int ObjLoader::getEdgeSize( const string & name )
{
	std::shared_ptr<Object> objectPtr = findObject(name);
	return objectPtr->edgeSize;
}


bool ObjLoader::replaceData( const std::string & strData )
{
	std::string::size_type startPos = 0, endPos;

	std::vector<QVector3D> vertexVectorArray;
	std::vector<QVector3D> normalVectorArray;
	std::vector<QVector2D> textureVectorArray;

	std::vector<unsigned int> indexVertexArray;
	std::vector<unsigned int> indexNormalArray;
	std::vector<unsigned int> indexTextureArray;

	unsigned int globalEdgeSize = 0;

	std::shared_ptr<Object> objectPtr (new Object);
	while ( (endPos = strData.find('\n', startPos)) != std::string::npos )
	{
		unsigned int lengthLine = endPos - startPos;
		if ('o' == strData[startPos])
		{
			++startPos;
			// if (objectPtr->name.size())
			// {
			// 	cashNameArray.push_back(objectPtr->name);
			// 	objectMap[objectPtr->name] = objectPtr;
			// 	objectPtr.reset(new Object);
			// }
			objectPtr->name = string(&strData[startPos], lengthLine);
		}
		else if ('v' == strData[startPos])
		{
			++startPos;
			if ('t' == strData[startPos])
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				QVector2D point;
				point.setX( std::strtof(start, &end) );
				start = end;
				point.setY( std::strtof(start, &end) );

				textureVectorArray.push_back(point);
			}
			else if ('n' == strData[startPos])
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				float xn, yn, zn;
				xn = std::strtof(start, &end);
				start = end;
				yn = std::strtof(start, &end);
				start = end;
				zn = std::strtof(start, &end);

				float dividor = std::sqrt(xn*xn + yn*yn + zn*zn);
				xn /= dividor;
				yn /= dividor;
				zn /= dividor;

				normalVectorArray.push_back(QVector3D(xn, yn, zn));
			}
			else if ((' ' == strData[startPos]) || ('\t' == strData[startPos]))
			{
				++startPos;
				const char *start = &strData[startPos];
				char *end;
				float x, y, z;
				x = std::strtof(start, &end);
				start = end;
				y = std::strtof(start, &end);
				start = end;
				z = std::strtof(start, &end);

				vertexVectorArray.push_back(QVector3D(x, y, z));
			}
		}
		else if ('f' == strData[startPos])
		{
			++startPos;
			std::string line = strData.substr(startPos, endPos);
			std::stringstream streamLine(line);
			unsigned int localEdgeSize = 0;

			unsigned int point;
			while (1)
			{
				streamLine >> point;
				if (!streamLine.fail()) {
					indexVertexArray.push_back(point);
					++localEdgeSize;
				}
				else {

					if (localEdgeSize == 4)
					{
						unsigned int f1, f3;
						auto iter = indexVertexArray.rbegin();

						++iter;
						f3 = *iter;
						iter += 2;
						f1 = *iter;
						indexVertexArray.push_back(f3);
						indexVertexArray.push_back(f1);

						iter = indexNormalArray.rbegin();
						++iter;
						f3 = *iter;
						iter += 2;
						f1 = *iter;
						indexNormalArray.push_back(f3);
						indexNormalArray.push_back(f1);

						if (indexTextureArray.size())
						{
							iter = indexTextureArray.rbegin();
							++iter;
							f3 = *iter;
							iter += 2;
							f1 = *iter;
							indexTextureArray.push_back(f3);
							indexTextureArray.push_back(f1);
						}

						localEdgeSize = 3;
					}


					if (globalEdgeSize == 0)
					{
						globalEdgeSize = localEdgeSize;
					}
					else if (globalEdgeSize != localEdgeSize)
					{
						std::cerr << "(globalEdgeSize != localEdgeSize)  "
						          << globalEdgeSize << "  " << localEdgeSize
						          <<  '\n' << line;
						return false;
					}
					break;
				}

				size_t pos = streamLine.tellg();
				streamLine.ignore(line.size() - pos - 1, '/');
				if ( streamLine.tellg() < (line.size() - 1) )
				{
					streamLine >> point;
					if (streamLine.good()) {
						indexTextureArray.push_back(point);
					}
					else {
						streamLine.clear();
						streamLine.ignore(1);;
					}

					size_t pos = streamLine.tellg();
					streamLine.ignore(line.size() - pos - 1, '/');
					streamLine >> point;
					indexNormalArray.push_back(point);
				}
				else {
					streamLine.seekg(pos);
				}
			}
		}
		startPos = endPos + 1;
	}

	size_t vs = vertexVectorArray.size();
	size_t ns = normalVectorArray.size();
	size_t ts = textureVectorArray.size();

	size_t ivs = indexVertexArray.size();
	size_t ins = indexNormalArray.size();
	size_t its = indexTextureArray.size();

	if ((ins == 0) || (ins != ivs))
	{
		indexNormalArray = indexVertexArray;
		for (size_t i = 0; i < ivs; i+=3)
		{
			size_t ia = indexVertexArray[i] - 1;
			size_t ib = indexVertexArray[i+1] - 1;
			size_t ic = indexVertexArray[i+2] - 1;

			QVector3D normal = QVector3D::crossProduct( vertexVectorArray[ib] - vertexVectorArray[ia],
			                                            vertexVectorArray[ic] - vertexVectorArray[ia] );
			normal.normalize();

			normalVectorArray.resize(ivs, QVector3D(0.0, 0.0, 0.0));
			std::vector<int> nb_seen(vs, 0);
			size_t v[3];  v[0] = ia;  v[1] = ib;  v[2] = ic;
			for (int j = 0; j < 3; j++)
			{
				size_t cur_v = v[j];
				nb_seen[cur_v]++;
				if (nb_seen[cur_v] == 1)
				{
					normalVectorArray[cur_v] = normal;
				} else
				{
					// average
					normalVectorArray[cur_v].setX( normalVectorArray[cur_v].x() * (1.0 - 1.0/nb_seen[cur_v]) + normal.x() * 1.0/nb_seen[cur_v] );
					normalVectorArray[cur_v].setY( normalVectorArray[cur_v].y() * (1.0 - 1.0/nb_seen[cur_v]) + normal.y() * 1.0/nb_seen[cur_v] );
					normalVectorArray[cur_v].setZ( normalVectorArray[cur_v].z() * (1.0 - 1.0/nb_seen[cur_v]) + normal.z() * 1.0/nb_seen[cur_v] );
					normalVectorArray[cur_v].normalize();
				}
			}
		}
		ins = ivs;
		ns = vs;
	}
	if ((its == 0) || (its != ivs))
	{
		indexTextureArray.resize(0);
		indexTextureArray.assign(ivs, 0);
		textureVectorArray.resize(0);
		its = ivs;
	}

	if (ivs == 0)
	{
		fprintf(stderr, "size problem: ivs = %lu ; its = %lu ; ins = %lu\n", ivs, its, ins);
		return false;
	}

	ObjLoader::IndexBox indexBox;
	std::set<ObjLoader::IndexBox> indexSet;
	objectPtr->edgeSize = globalEdgeSize;

	for (size_t i = 0; i < ivs; ++i)
	{
		indexBox.index = indexSet.size();
		indexBox.vertex = indexVertexArray[i];
		indexBox.normal = indexNormalArray[i];
		indexBox.texture = indexTextureArray[i];

		auto iter = indexSet.find(indexBox);
		if (iter == indexSet.end())
		{
			indexSet.insert(indexBox);
			objectPtr->indexArray.push_back(indexBox.index);

			if ((vs < indexBox.vertex) || (ns < indexBox.normal) || (ts < indexBox.texture))
			{
				std::cerr << "index is too big"
				          << "\n  vs = " << vs << "  vind = " << indexBox.vertex
				          << "\n  ns = " << ns << "  nind = " << indexBox.normal
				          << "\n  ts = " << ns << "  tind = " << indexBox.texture;
				return false;
			}

			if (0 != indexBox.vertex) {
				const QVector3D & vertex = vertexVectorArray[indexBox.vertex-1];
				objectPtr->vertexArray.push_back(vertex);
			}
			if (0 != indexBox.normal) {
				const QVector3D & normal = normalVectorArray[indexBox.normal-1];
				objectPtr->normalArray.push_back(normal);
			}
			if (0 != indexBox.texture) {
				const QVector2D & texture = textureVectorArray[indexBox.texture-1];
				objectPtr->textureArray.push_back(texture);
			}
		}
		else
		{
			indexBox = *iter;
			objectPtr->indexArray.push_back(indexBox.index);
		}
	}

	objectPtr->normalize();
	objectPtr->indexArray.insert(objectPtr->indexArray.end(),
	                             objectPtr->indexArray.rbegin(),
	                             objectPtr->indexArray.rend());

	cashNameArray.push_back(objectPtr->name);
	objectMap[objectPtr->name] = objectPtr;

	return true;
}



// ---------  --------- //


std::shared_ptr<ObjLoader::Object> ObjLoader::findObject( const string & name )
{
	std::shared_ptr<ObjLoader::Object> objectPtr;

	auto iter = objectMap.find(name);
	if ( iter == objectMap.end() )
	{
		cashNameArray.push_back(name);
		objectPtr.reset(new ObjLoader::Object(name));
		objectPtr->edgeSize = 0;
		objectMap[name] = objectPtr;
	}
	else
	{
		objectPtr = iter->second;
	}

	return objectPtr;
}



ObjLoader::Object::Object( ObjLoader::Object && other)
{
	std::swap(name, other.name);
	std::swap(edgeSize, other.edgeSize);
	std::swap(vertexArray, other.vertexArray);
	std::swap(normalArray, other.normalArray);
	std::swap(textureArray, other.textureArray);
	std::swap(indexArray, other.indexArray);
}

void ObjLoader::Object::normalize()
{
	float max_abs = 0;
	for (size_t i = 0; i < vertexArray.size(); ++i)
	{
		max_abs = std::max(max_abs, vertexArray[i].length());
	}

	for (size_t i = 0; i < vertexArray.size(); ++i)
	{
		vertexArray[i] /= max_abs;
	}
}


bool operator==( const ObjLoader::Object & first, const ObjLoader::Object & scnd )
{
	bool equal = (first.name == scnd.name) && (first.edgeSize == scnd.edgeSize);
	if (equal)
	{
		equal = first.vertexArray == scnd.vertexArray;
	}
	if (equal)
	{
		equal = first.normalArray == scnd.normalArray;
	}
	if (equal)
	{
		equal = first.textureArray == scnd.textureArray;
	}
	if (equal)
	{
		equal = first.indexArray == scnd.indexArray;
	}

	return equal;
}


bool operator<( const ObjLoader::Object & first, const ObjLoader::Object & scnd )
{
	return (first.name < scnd.name);
}

bool operator<( const ObjLoader::Object & first, const std::string & name )
{
	return (first.name < name);
}


bool operator<(const ObjLoader::IndexBox &first, const ObjLoader::IndexBox &scnd )
{
	bool less = true;
	if (!(first.vertex < scnd.vertex))
	{
		if (!(first.normal < scnd.normal))
		{
			if (!(first.texture < scnd.texture))
			{
				less = false;
			}
		}
	}
	return less;
}


