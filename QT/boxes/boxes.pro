QT += opengl widgets

contains(QT_CONFIG, opengles.|angle|dynamicgl):error("This example requires Qt to be configured with -opengl desktop")

QMAKE_CXXFLAGS += -std=c++11

HEADERS += 3rdparty/fbm.h \
           glbuffers.h \
           glextensions.h \
           gltrianglemesh.h \
           roundedbox.h \
           scene.h \
           trackball.h \
    p3t2n3vertex.h \
    objloader.h \
    objbox.h \
    phisics.h
SOURCES += 3rdparty/fbm.c \
           glbuffers.cpp \
           glextensions.cpp \
           main.cpp \
           roundedbox.cpp \
           scene.cpp \
           trackball.cpp \
    p3t2n3vertex.cpp \
    objloader.cpp \
    objbox.cpp \
    phisics.cpp

RESOURCES += boxes.qrc \
    shaders.qrc \
    objects.qrc


wince {
    DEPLOYMENT_PLUGIN += qjpeg
}
