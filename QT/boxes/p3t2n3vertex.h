#ifndef P3T2N3VERTEX_H
#define P3T2N3VERTEX_H

#include "glextensions.h"

#include <QtWidgets>
#include <QtOpenGL>

#include "gltrianglemesh.h"
#include <QtGui/qvector3d.h>
#include <QtGui/qvector2d.h>
#include "glbuffers.h"


struct P3T2N3Vertex
{
	  QVector3D position;
		QVector2D texCoord;
		QVector3D normal;
		static VertexDescription description[];
};

#endif // P3T2N3VERTEX_H
