
#include "p3t2n3vertex.h"

//============================================================================//
//                                P3T2N3Vertex                                //
//============================================================================//

VertexDescription P3T2N3Vertex::description[] = {
  {VertexDescription::Position, GL_FLOAT, SIZE_OF_MEMBER(P3T2N3Vertex, position) / sizeof(float), 0, 0},
  {VertexDescription::TexCoord, GL_FLOAT, SIZE_OF_MEMBER(P3T2N3Vertex, texCoord) / sizeof(float), sizeof(QVector3D), 0},
  {VertexDescription::Normal, GL_FLOAT, SIZE_OF_MEMBER(P3T2N3Vertex, normal) / sizeof(float), sizeof(QVector3D) + sizeof(QVector2D), 0},

  {VertexDescription::Null, 0, 0, 0, 0},
};

