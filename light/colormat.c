
#include "colormat.h"
#include "viewer.h"
#include "listdir.h"

#include <math.h>
#include <stdio.h>


void init()
{
	GLfloat mat_shininess[] = { 100.0 };
	glMaterialfv( GL_FRONT, GL_SHININESS, mat_shininess );

	showShadowMap = 0;
	shadowSize = displayWidth;
	eyeViewer = init_view( 40 );
	localViewer = eyeViewer;
	viewerId = 0;

	glClearColor( 0, 0, 0, 1 );

	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LEQUAL );
	glAlphaFunc( GL_GREATER, 0.7f );
	glPolygonOffset( 1.0, 0.0 );

	glShadeModel( GL_SMOOTH );
	glEnable( GL_LIGHTING );
	glEnable( GL_NORMALIZE );
	glEnable( GL_TEXTURE_2D );

	/* glCullFace(GL_BACK); */
	/* glFrontFace(GL_CCW); */
	/* glEnable(GL_CULL_FACE); */

	initLight();
	initDrawObjects();
}


void display()
{
	for (size_t i = 0; i < amountLight; ++i)
	{
		GLuint lightEnum = GL_LIGHT0 + i;
		glLightfv( lightEnum, GL_POSITION, lightPosition[i] );

		/* const double * xyz = get_xyz_view( lightViewer[i] ); */
		/* printf("disp - %f, %f, %f\n\n\n", xyz[0], xyz[1], xyz[2]); */

		if (!isLightTextureFixed[i]) {
			fillTextureFromDisplay( lightTextureId[i], lightPosition[i],
			                        lightTextureListConstructorId[i], lightTextureListDestructorId[i]);
			isLightTextureFixed[i] = GL_TRUE;
		}
	}

	reprint();

	const double * xyz = get_xyz_view( eyeViewer );
	glLoadIdentity();
	gluLookAt( xyz[0], xyz[1], xyz[2],
	           0.0, 0.0, 0.0,
	           0, 1, 0 );

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	if ( showShadowMap ) {
		/* for (size_t i = 0; i < amountLight; ++i) { */
		/* 	GLuint lightEnum = GL_LIGHT0 + i; */

		/* 	GLfloat lightZero[4] = { 0, 0, 0, 1 }; */
		/* 	GLfloat lowAmbient[4] = { 0.05, 0.05, 0.05, 1.0 }; */
		/* 	glLightfv( lightEnum, GL_SPECULAR, lightZero ); */
		/* 	glLightfv( lightEnum, GL_AMBIENT, lowAmbient ); */
		/* 	glLightfv( lightEnum, GL_DIFFUSE, lightZero ); */

		/* 	drawObjects(); */

		/* 	glLightfv( lightEnum, GL_SPECULAR, lightSpecular[i] ); */
		/* 	glLightfv( lightEnum, GL_AMBIENT, lightAmbient[i] ); */
		/* 	glLightfv( lightEnum, GL_DIFFUSE, lightDiffuse[i] ); */
		/* } */

		for (size_t i = 0; i < amountLight; ++i) {

			GLuint lightEnum = GL_LIGHT0 + i;

			GLfloat sPlane[4] = { 1, 0, 0, 0 };
			GLfloat tPlane[4] = { 0, 1, 0, 0 };
			GLfloat rPlane[4] = { 0, 0, 1, 0 };
			GLfloat qPlane[4] = { 0, 0, 0, 1 };

			glBindTexture( GL_TEXTURE_2D, lightTextureId[i] );
			glEnable( GL_TEXTURE_2D );
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

			glEnable( GL_ALPHA_TEST );
			glEnable( GL_TEXTURE_GEN_S );
			glEnable( GL_TEXTURE_GEN_T );
			glEnable( GL_TEXTURE_GEN_R );
			glEnable( GL_TEXTURE_GEN_Q );
			glTexGenfv( GL_S, GL_EYE_PLANE, sPlane );
			glTexGenfv( GL_T, GL_EYE_PLANE, tPlane );
			glTexGenfv( GL_R, GL_EYE_PLANE, rPlane );
			glTexGenfv( GL_Q, GL_EYE_PLANE, qPlane );

			drawObjects();

			glDisable( GL_ALPHA_TEST );
			glDisable( GL_TEXTURE_2D );
			glDisable( GL_TEXTURE_GEN_S );
			glDisable( GL_TEXTURE_GEN_T );
			glDisable( GL_TEXTURE_GEN_R );
			glDisable( GL_TEXTURE_GEN_Q );
		}
	}
	else {
		drawObjects();
	}

	glutSwapBuffers();
}


void reshape( int w, int h )
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glLoadIdentity();
}


void keyboard( unsigned char key, int x, int y )
{
	modify_view( localViewer, key );
	if (viewerId != 0) {
		isLightTextureFixed[viewerId-1] = GL_FALSE;

		const double * xyz = get_xyz_view( localViewer );
		lightPosition[viewerId-1][0] = xyz[0];
		lightPosition[viewerId-1][1] = xyz[1];
		lightPosition[viewerId-1][2] = xyz[2];

		/* printf("vid = %ui \n", viewerId); */
		/* printf("%f, %f, %f \n\n", */
		/*        lightPosition[viewerId-1][0], */
		/*        lightPosition[viewerId-1][1], */
		/*        lightPosition[viewerId-1][2]); */
	}

	switch (key) {
	case 'q':
		glutLeaveMainLoop();
		break;
	case 's':
		showShadowMap = !showShadowMap;
		break;
	case 'n':
		++viewerId;
		if (amountLight < viewerId) {
			viewerId = 0;
		}
		if (viewerId == 0) {
			localViewer = eyeViewer;
		}
		else {
			localViewer = lightViewer[viewerId-1];
		}
	}
	glutPostRedisplay();
}


int main(int argc, char *argv[])
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitWindowSize( displayWidth, displayHeight );
	glutInitWindowPosition( start_position_x, start_position_y );
	glutCreateWindow( argv[0] );

	init();

	glutDisplayFunc( display );
	glutReshapeFunc( reshape );
	glutKeyboardFunc( keyboard );
	glutMainLoop();

	return 0;
}



void initLight()
{
	amountLight = 2;
	lightTextureId = malloc(sizeof(GLuint) * amountLight);
	lightTextureListConstructorId  = malloc(sizeof(GLuint) * amountLight);
	lightTextureListDestructorId = malloc(sizeof(GLuint) * amountLight);
	isLightTextureFixed = malloc(sizeof(GLuint) * amountLight);
	lightPosition = malloc(sizeof(GLfloat*) * amountLight);
	lightSpecular = malloc(sizeof(GLfloat*) * amountLight);
	lightAmbient  = malloc(sizeof(GLfloat*) * amountLight);
	lightDiffuse  = malloc(sizeof(GLfloat*) * amountLight);
	lightViewer  = malloc(sizeof(GLfloat*) * amountLight);

	for (size_t i = 0; i < amountLight; ++i)
	{
		GLuint lightEnum = GL_LIGHT0 + i;
		glEnable( lightEnum );

		glGenTextures( 1, &lightTextureId[i] );
		glBindTexture( GL_TEXTURE_2D, lightTextureId[i] );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
		glTexParameteri( GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY );
		glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FAIL_VALUE_ARB, 0.5 );
		glTexGeni( GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR );
		glTexGeni( GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR );
		glTexGeni( GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR );
		glTexGeni( GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

		lightTextureListConstructorId[i] = glGenLists(1);
		lightTextureListDestructorId[i] = glGenLists(1);
		genGuards( i, lightTextureListConstructorId[i], lightTextureListDestructorId[i] );

		lightPosition[i] = malloc(sizeof(GLfloat) * 4 );
		lightSpecular[i] = malloc(sizeof(GLfloat) * 4 );
		lightAmbient[i]  = malloc(sizeof(GLfloat) * 4 );
		lightDiffuse[i]  = malloc(sizeof(GLfloat) * 4 );
		genLightPosition( i, lightPosition[i] );
		genLightParam( i, lightSpecular[i], lightAmbient[i], lightDiffuse[i] );
		glLightfv( lightEnum, GL_SPECULAR, lightSpecular[i] );
		glLightfv( lightEnum, GL_AMBIENT, lightAmbient[i] );
		glLightfv( lightEnum, GL_DIFFUSE, lightDiffuse[i] );


		fillTextureFromDisplay( lightTextureId[i], lightPosition[i],
		                        lightTextureListConstructorId[i], lightTextureListDestructorId[i]);
		isLightTextureFixed[i] = GL_TRUE;

		GLuint lightTextureEnub = GL_TEXTURE0 + i;
		glActiveTexture(lightTextureEnub);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, lightTextureId[i]);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		lightViewer[i] = full_init_view_f( 1, 5, lightPosition[i] );
	}
}



void reprint()
{
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( 15, 1.2, 1, 1000 );

	glMatrixMode( GL_MODELVIEW );
}


void initDrawObjects()
{
	drawListId = glGenLists(1);
	glNewList( drawListId, GL_COMPILE );
	{
		GLfloat mat_zero[] = { 0, 0, 0, 1 };

		/* sphere */
		glPushMatrix();
		{
			GLfloat mat_solid[] = { 0.75, 0.75, 0.0, 1.0 };
			GLfloat mat_specular[] = { 1, 1, 1, 1 };

			glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
			glMaterialfv( GL_FRONT, GL_AMBIENT, mat_solid );
			glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_solid );
			glMaterialfv( GL_FRONT, GL_EMISSION, mat_zero );

			glTranslatef( 0.7, 1, 2 );
			glutSolidSphere( 1.5, 100, 100 );
		}
		glPopMatrix();


		/* cube */
		glPushMatrix();
		{
			GLfloat mat_solid[] = { 0.75, 0, 0.75, 1.0 };
			GLfloat mat_specular[] = { 1, 1, 1, 1 };

			glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
			glMaterialfv( GL_FRONT, GL_AMBIENT, mat_solid );
			glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_solid );
			glMaterialfv( GL_FRONT, GL_EMISSION, mat_zero );

			glTranslatef( -0.7, 0.5, -1.5 );
			glutSolidCube( 1.5 );
		}
		glPopMatrix();


		/* Tetrahedron */
		glPushMatrix();
		{
			GLfloat alpha = 0.8;

			glEnable( GL_BLEND );
			glBlendFunc( GL_SRC_ALPHA, GL_ONE );

			GLfloat mat_zero[4] = { 0.0, 0.0, 0.0, alpha };
			GLfloat mat_solid[4] = { 0.0, 0.8, 0.8, alpha };
			GLfloat mat_emission[4] = { 0.0, 0.08, 0.08, alpha };

			glMaterialfv( GL_FRONT, GL_SPECULAR, mat_zero );
			glMaterialfv( GL_FRONT, GL_AMBIENT, mat_solid );
			glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_solid );
			glMaterialfv( GL_FRONT, GL_EMISSION, mat_emission );

			glTranslatef( 1.7, 0, 0 );
			glScalef( 1.5, 1.5, 1.5 );
			glRotatef( 90, 0, 0, 1 );
			glutSolidTetrahedron();

			glDisable( GL_BLEND );
		}
		glPopMatrix();


		/* platform */
		glPushMatrix();
		{
			glEnable( GL_BLEND );
			GLfloat mat_solid[] = { 0.5, 0.3, 0.3, 1.0 };

			glMaterialfv( GL_FRONT, GL_SPECULAR, mat_zero );
			glMaterialfv( GL_FRONT, GL_AMBIENT, mat_solid );
			glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_solid );
			glMaterialfv( GL_FRONT, GL_EMISSION, mat_zero );

			glTranslatef( 0, -0.7, 0);
			glScalef( 10, 0.1, 10);
			glutSolidCube( 1 );

			glDisable( GL_BLEND );
		}
		glPopMatrix();

	}
	glEndList();
}


void drawObjects()
{
	GLboolean isAlphaTest = glIsEnabled( GL_ALPHA_TEST );
	GLboolean isDarkSide = glIsEnabled( GL_COLOR_MATERIAL );
	GLint globalAlphaFunc;
	GLfloat globalAlphaRef;

	glGetIntegerv( GL_ALPHA_TEST_FUNC, &globalAlphaFunc );
	glGetFloatv( GL_ALPHA_TEST_REF, &globalAlphaRef );

	if ( isAlphaTest && !isDarkSide ) {
		glCallList( drawListId );
	}
	else {
		glEnable( GL_ALPHA_TEST );
		glAlphaFunc( GL_GREATER, 0.9f );
		glCallList( drawListId );

		glAlphaFunc( GL_LEQUAL, 0.9f );
		glCallList( drawListId );
		glDisable( GL_ALPHA_TEST );

		glAlphaFunc( globalAlphaFunc, globalAlphaRef );
	}


	/* lighting detectors */
	GLfloat mat_zero[4] = { 0, 0, 0, 1 };
	for (size_t i = 0; i < amountLight; ++i)
	{
		glPushMatrix();
		{
			glMaterialfv( GL_FRONT, GL_SPECULAR, mat_zero );
			glMaterialfv( GL_FRONT, GL_AMBIENT, mat_zero );
			glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_zero );
			glMaterialfv( GL_FRONT, GL_EMISSION, lightSpecular[i] );

			glTranslatef( lightPosition[i][0], lightPosition[i][1], lightPosition[i][2] );
			glutSolidSphere( 0.1, 100, 100 );
		}
		glPopMatrix();
	}
}



void fillTextureFromDisplay( GLuint textureId, const GLfloat *position,
                             GLuint listConstructor, GLuint listDestructor )
{
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	{
		GLfloat toSceneDistance, nearPlane, fieldOfView;
		GLfloat lightModelview[16], lightProjection[16];
		float deep = 30;
		float radius = deep / 2;

		toSceneDistance = get_distance_f( position );

		nearPlane = toSceneDistance - radius;
		if ( nearPlane < 1.0f ) {
			nearPlane = 1.0f;
		}

		if ( toSceneDistance < radius ) {
			fieldOfView = 90;
		}
		else {
			float pi = 3.14159;
			fieldOfView = asin( radius / toSceneDistance );
			fieldOfView *= 180 / pi;
		}
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		gluPerspective( fieldOfView, 1, nearPlane, nearPlane + deep );
		glGetFloatv( GL_PROJECTION_MATRIX, lightProjection );

		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();
		gluLookAt( position[0], position[1], position[2],
		           0, 0, 0,
		           0, 1, 0 );
		glGetFloatv( GL_MODELVIEW_MATRIX, lightModelview );

		glClear( GL_DEPTH_BUFFER_BIT );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		if ( listConstructor != 0 ) {
			glCallList( listConstructor );
		}
		{
			drawObjects();

			GLint level = 0;
			GLint internalFormat = GL_DEPTH_COMPONENT; // GL_RGBA;
			GLint xOffset = 0, yOffset = 0;

			glBindTexture( GL_TEXTURE_2D, textureId );
			glCopyTexImage2D( GL_TEXTURE_2D, level, internalFormat,
			                  xOffset, yOffset,
			                  shadowSize, shadowSize, 0 );
		}
		if ( listDestructor != 0 ) {
			glCallList( listDestructor );
		}

		glMatrixMode( GL_TEXTURE );
		glLoadIdentity();
		glTranslatef( 0.5, 0.5, 0.5 );
		glScalef( 0.5, 0.5, 0.5 );
		glMultMatrixf( lightProjection );
		glMultMatrixf( lightModelview );

	}
	glMatrixMode( GL_MODELVIEW );
}




void genLightParam( unsigned int i,
                    GLfloat *lightSpecular,
                    GLfloat *lightAmbient,
                    GLfloat *lightDiffuse )
{
	if ( 1 || (i == 0) ) {
		lightSpecular[0] = 1.0;
		lightSpecular[1] = 0.0;
		lightSpecular[2] = 0.0;
		lightSpecular[3] = 1.0;

		lightAmbient[0] = 0.0;
		lightAmbient[1] = 0.0;
		lightAmbient[2] = 0.0;
		lightAmbient[3] = 1.0;

		lightDiffuse[0] = 0.35;
		lightDiffuse[1] = 0.35;
		lightDiffuse[2] = 0.35;
		lightDiffuse[3] = 1.0;
	}
}

void genLightPosition( unsigned int i, GLfloat *lightPosition)
{
	if ( 1 || (i == 0) ) {
		lightPosition[0] = -0.5;
		lightPosition[1] =  3.0;
		lightPosition[2] =  15.0;
		lightPosition[3] =  1.0;
	}
}


void genGuards( unsigned int i,
                GLuint constructorListId,
                GLuint destructorListId )
{
	if ( 1 || (i == 0) ) {
		glNewList( constructorListId, GL_COMPILE );
		{
			glShadeModel( GL_FLAT );
			glDisable( GL_LIGHTING );
			glDisable( GL_NORMALIZE );
			glColorMask( 0, 0, 0, 0 );
			glEnable( GL_POLYGON_OFFSET_FILL );
		}
		glEndList();

		glNewList( destructorListId, GL_COMPILE );
		{
			glShadeModel( GL_SMOOTH );
			glEnable( GL_LIGHTING );
			glEnable( GL_NORMALIZE );
			glColorMask( 1, 1, 1, 1 );
			glDisable( GL_POLYGON_OFFSET_FILL );
		}
		glEndList();
	}
}
