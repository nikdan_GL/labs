#ifndef LIGHT_COLORMAT_H
#define LIGHT_COLORMAT_H

#include <GL/freeglut.h>


static unsigned int displayWidth = 1200;
static unsigned int displayHeight = 1000;

const unsigned int start_position_x = 100;
const unsigned int start_position_y = 100;

static struct viewer_t * eyeViewer;
static struct viewer_t * localViewer;
static unsigned int viewerId;

static int showShadowMap;
static int shadowSize;

static GLuint amountLight;			                 // must be < 8
static GLuint * lightTextureId;
static GLuint * lightTextureListConstructorId;
static GLuint * lightTextureListDestructorId;
static GLboolean * isLightTextureFixed;

static GLfloat ** lightPosition;
static GLfloat ** lightSpecular;
static GLfloat ** lightAmbient;
static GLfloat ** lightDiffuse;
static struct viewer_t ** lightViewer;

static GLuint drawListId;



void init();
void display();
void reshape( int w, int h );
void keyboard( unsigned char key, int x, int y );


void initLight();
void reprint();
void initDrawObjects();
void drawObjects();


void fillTextureFromDisplay( GLuint textureId, const GLfloat *position,
                             GLuint listConstructor, GLuint listDestructor );


void subInitDrawObjects( GLboolean blending );


void genLightParam( unsigned int i,
                    GLfloat *lightSpecular,
                    GLfloat *lightAmbient,
                    GLfloat *lightDiffuse );

void genLightPosition( unsigned int i, GLfloat *lightPosition);

void genGuards( unsigned int i,
                GLuint lightConstructorId,
                GLuint lightDestructorId );


#endif /* LIGHT_COLORMAT_H */
