#ifndef INCLUDE_LISTDIR_H
#define INCLUDE_LISTDIR_H

#include "list.h"

struct list_t * list_dir( const char *path );


#endif /* INCLUDE_LISTDIR_H */
