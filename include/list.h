#ifndef INCLUDE_LIST_STR_H
#define INCLUDE_LIST_STR_H

struct list_t
{
	struct list_t * next;

	int full;
	int elem_size;
	void * elem;
};


struct list_t * create_list();

void add( struct list_t * head, int elem_size, void * elem );
void * get( struct list_t * head, int number);
int get_amount( struct list_t *head );

int remove_n( struct list_t * head, int number );
int remove_p( struct list_t * head, const void * elem );

void clear( struct list_t * head );


#endif /* INCLUDE_LIST_STR_H */
