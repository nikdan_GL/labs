
#include "viewer.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PI 3.14159


struct viewer_t {
	double dist_step;
	double angle_step;

	double point_angle[3];
	double position[3];

	double height_width_deep[3];
};


double get_distance_d( const double * position );
float get_distance_f( const float * position );

double get_azimuth( const double * position );
double get_angle_place( const double * position );

double rad2deg( double rad );
double deg2rad( double deg );



struct viewer_t * init_view( float distance )
{
	double dist_step = 1;
	double angle_step = 5;

	double point_angle[3] = {distance, 0, 0};
	double position[3];
	angle2xyz( point_angle, position );

	return full_init_view_d( dist_step, angle_step, position );
}


struct viewer_t * full_init_view_d( double dist_step, double angle_step,
                                    const double * position )
{
	struct viewer_t * viewer = malloc(sizeof(struct viewer_t));

	viewer->dist_step = dist_step;
	viewer->angle_step = angle_step;

	memcpy( viewer->position, position, sizeof(double)*3 );
	xyz2angle( position, viewer->point_angle );

	viewer->height_width_deep[0] = 20 * viewer->point_angle[0];
	viewer->height_width_deep[1] = 20 * viewer->point_angle[0];
	viewer->height_width_deep[2] = 50;

	return viewer;
}


struct viewer_t * full_init_view_f( float dist_step, float angle_step,
                                    const float * position )
{
	double d_position[] = {position[0], position[1], position[2]};
	return full_init_view_d( dist_step, angle_step, d_position );
}




const double * get_xyz_view( const struct viewer_t * viewer )
{
	return viewer->position;
}

const double * get_hvd_view( const struct viewer_t * viewer )
{
	return viewer->height_width_deep;
}


void modify_view( struct viewer_t * viewer, unsigned char key )
{
	angle2xyz( viewer->point_angle, viewer->position );

	switch (key) {
	case 'h': {
		viewer->point_angle[1] += viewer->angle_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}
	case 'l': {
		viewer->point_angle[1] -= viewer->angle_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}

	case 'j': {
		viewer->point_angle[2] -= viewer->angle_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}
	case 'k': {
		viewer->point_angle[2] += viewer->angle_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}

	case 'f': {
		viewer->point_angle[0] -= viewer->dist_step;
		viewer->height_width_deep[0] -= viewer->dist_step;
		viewer->height_width_deep[1] -= viewer->dist_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}
	case 'b': {
		viewer->point_angle[0] += viewer->dist_step;
		viewer->height_width_deep[0] += viewer->dist_step;
		viewer->height_width_deep[1] += viewer->dist_step;
		angle2xyz( viewer->point_angle, viewer->position );
		break;
	}
	}
}



void angle2xyz( const double * src_position, double * dst_position )
{
	double local_distance = src_position[0];
	double local_azimuth = deg2rad( src_position[1] );
	double local_angle_place = deg2rad( src_position[2] );

	double y = sin(local_angle_place) * local_distance;

	double x = sin(local_azimuth) * local_distance * cos(local_angle_place);
	double z = cos(local_azimuth) * local_distance * cos(local_angle_place);

	dst_position[0] = x;
	dst_position[1] = y;
	dst_position[2] = z;

	/* printf("d = %e, az = %e, ap = %e\n", local_distance, local_azimuth, local_angle_place); */
	/* printf("x = %e, y = %e, z = %e\n\n", x, y, z); */
}


void xyz2angle( const double * src_position, double * dst_position )
{
	double local_distance = get_distance_d( src_position );
	double local_azimuth = get_azimuth( src_position );
	double local_angle_place = get_angle_place( src_position );

	dst_position[0] = local_distance;
	dst_position[1] = rad2deg( local_azimuth );
	dst_position[2] = rad2deg( local_angle_place );

	/* double x = src_position[0]; */
	/* double y = src_position[1]; */
	/* double z = src_position[2]; */
	/* printf("x = %e, y = %e, z = %e\n", x, y, z); */
	/* printf("d = %e, az = %e, ap = %e\n\n", local_distance, local_azimuth, local_angle_place); */
}




/*------  private   ------*/



double get_distance_d( const double * position )
{
	return sqrt(position[0]*position[0] + position[1]*position[1] + position[2]*position[2]);
}
float get_distance_f( const float * position )
{
	return sqrt(position[0]*position[0] + position[1]*position[1] + position[2]*position[2]);
}


double get_azimuth( const double * position )
{
	if (position[2] == 0) {
		return (position[0] < 0) ? -PI/2.0 : PI/2.0;
	}
	return atan( position[0] / position[2]);
}

double get_angle_place( const double * position )
{
	return asin(position[1] / get_distance_d(position));
}


double rad2deg( double rad )
{
	return rad * 360 / (2*PI);
}
double deg2rad( double deg )
{
	return deg / 360 * (2*PI);
}
