#ifndef VIEWER_VIEWER_H
#define VIEWER_VIEWER_H

struct viewer_t;


struct viewer_t * init_view( float distance );
struct viewer_t * full_init_view_d( double dist_step, double angle_step,
                                    const double * position );
struct viewer_t * full_init_view_f( float dist_step, float angle_step,
                                    const float * position );

const double * get_xyz_view( const struct viewer_t * viewer );
const double * get_hvd_view( const struct viewer_t * viewer );

void modify_view( struct viewer_t * viewer, unsigned char key );



double get_distance_d( const double * position );
float  get_distance_f( const float * position );


void angle2xyz( const double * src_position, double * dst_position );
void xyz2angle( const double * src_position, double * dst_position );


#endif /* VIEWER_VIEWER_H */
