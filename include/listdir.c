
#include "listdir.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>


struct list_t * list_dir( const char *path )
{
	struct dirent *entry;
	DIR *dp;
	struct list_t *head = NULL;

	dp = opendir(path);
	if (dp == NULL) {
		perror("opendir");
		return head;
	}
	head = create_list();

	while ((entry = readdir(dp))) {
		if (entry->d_name[0] != '.') {
			const int str_size = (strlen(entry->d_name)+1);
			char *file_name = (char*)malloc( sizeof(char)*str_size );
			strcpy(file_name, entry->d_name);
			add( head, str_size, (void*)file_name );
		}
	}

	closedir(dp);
	return head;
}
