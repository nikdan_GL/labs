
#include "list.h"
#include <stdlib.h>


struct list_t * create_list()
{
	struct list_t * head = (struct list_t *)malloc(sizeof(struct list_t));
	head->next = NULL;
	head->full = 0;
	return head;
}


void add( struct list_t * head, int elem_size, void * elem )
{
	if (head) {
		struct list_t * walker = head;
		while (walker->next != NULL) {
			walker = walker->next;
		}
		if (walker->full) {
			walker->next = (struct list_t *)malloc( sizeof(struct list_t) );
			walker = walker->next;
		}

		walker->full = 1;
		walker->elem_size = elem_size;
		walker->elem = elem;
		walker->next = NULL;
	}
}


void * get( struct list_t * head, int number )
{
	void * elem = NULL;
	if (head && (0 <= number)) {
		struct list_t * walker = head;
		int counter = -1;
		while ( (++counter != number) && (walker->next != NULL) ) {
			walker = walker->next;
		}

		if ((counter == number) && (walker->full)) {
			elem = walker->elem;
		}
	}
	return elem;
}


int get_amount( struct list_t *head )
{
	int amount = 0;
	struct list_t * walker = head;
	while ( walker != NULL) {
		if (walker->full) {
			++amount;
		}
		walker = walker->next;
	}
	return amount;
}



/* ------ removable block start ----- */
void remove( struct list_t * );

int remove_n( struct list_t * head, int number )
{
	int is_removed = 0;
	if (head && (0 <= number)) {
		struct list_t * walker = head;
		int counter = -1;
		while ( (++counter != number) && (walker->next != NULL) ) {
			walker = walker->next;
		}

		if ( (counter == number) && (walker->full) ) {
			is_removed = 1;
			remove( walker );
		}
	}
	return is_removed;
}

int remove_p( struct list_t * head, const void * elem )
{
	int is_removed = 0;
	if (head) {
		struct list_t * walker = head;
		while ( walker->next != NULL ) {
			if ( (walker->elem == elem) && (walker->full) ) {
				++is_removed;
				remove( walker );
			}
			walker = walker->next;
		}
	}
	return is_removed;
}

void remove( struct list_t *head )
{
	if (head->next == NULL) {
		head->full = 0;
	}
	else {
		struct list_t * target = head->next;

		head->full = target->full;
		head->elem_size = target->elem_size;
		head->elem = target->elem;
		head->next = target->next;

		free(target);
	}
}

/* ------ removable block end ----- */


void clear( struct list_t * head )
{
	while (head) {
		struct list_t * target = head;
		head = head->next;
		free(target);
	}
}
